<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Institution extends Model
{
    use Sortable;

    protected $fillable = [
        'name', 'reg_number', 'nip', 'krs', 'post_code', 'city', 'address', 'active'
    ];

    public $timestamps = false;

    public $sortable = ['id',
        'name',
        'city',
        'nip',
        'active'];
}
