<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Loan extends Model
{
    use Sortable;

    protected $fillable = [
        'name', 'goal', 'institution', 'borrower', 'borrower_id', 'conclusion_date', 'award_date', 'period', 'loan_amount', 'commitment_amount',
        'de_minimis', 'status', 'payoff_date', 'rating'
    ];

    public $sortable = ['id',
        'name',
        'loan_amount',
        'period',
        'status',
        'rating'];


    public function borrower()
    {
        return $this->belongsTo(Borrower::class);
    }
}
