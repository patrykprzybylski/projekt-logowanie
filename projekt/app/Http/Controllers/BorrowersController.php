<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Institution;
use Illuminate\Http\Request;
use Validator;
use View;

class BorrowersController extends Controller
{
    public function index(Request $request)
    {

        $borrowers = Borrower::sortable(['id' => 'desc'])->paginate(10);

        if($request->get('records'))
        {
            $borrowers = Borrower::sortable()->paginate($request->get('records'));
        }

        if ($request->get('active') === 'active')
        {
            $borrowers = Borrower::sortable()->where('active', 1)->paginate(10);
            $countBorrowers = count(Borrower::where('active', 1)->get());
            return response()->json(View::make('borrowers.list', array('borrowers' => $borrowers, 'countBorrowers' => $countBorrowers))->render());
        }

        if ($request->get('active') === 'not')
        {
            $borrowers = Borrower::sortable()->where('active', 0)->paginate(10);
            $countBorrowers = count(Borrower::where('active', 0)->get());
            return response()->json(View::make('borrowers.list', array('borrowers' => $borrowers, 'countBorrowers' => $countBorrowers))->render());
        }

        if($request->get('searchVal'))
        {
            $borrowers = Borrower::sortable()->where('name', 'like', '%' . $request->get('searchVal') . '%')->orWhere('city', 'like',  '%' . $request->get('searchVal') . '%')->paginate(10);
            $countBorrowers = count(Borrower::where('name', 'like', '%' . $request->get('searchVal') . '%')->orWhere('city', 'like',  '%' . $request->get('searchVal') . '%')->get());
            return response()->json(View::make('borrowers.list', array('borrowers' => $borrowers, 'countBorrowers' => $countBorrowers))->render());
        }

        if($request->ajax())
        {
            return response()->json(View::make('borrowers.list', array('borrowers' => $borrowers, 'countBorrowers' => count($borrowers)))->render());
        }

        return view('borrowers.index', array('borrowers' => $borrowers, 'allBorrowers' => count(Borrower::all())));

    }


    public function store(Request $request)
    {
        $rules = array(
            'name'      => ['required', 'string', 'max:255'],
            'post_code' => ['required'],
            'city'      => ['required'],
            'address'   => ['required']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }

        $form_data = array(
            'name'          => $request->name,
            'reg_number'    => $request->reg_number,
            'nip'           => $request->nip,
            'krs'           => $request->krs,
            'post_code'     => $request->post_code,
            'city'          => $request->city,
            'address'       => $request->address,
            'active'        => $request->active
        );

        $borrower = Borrower::create($form_data);
        return response()->json(['borrower' => $borrower]);
    }


    public function destroy($id)
    {
        $borrower = Borrower::findOrFail($id);
        $borrower->delete();

        return response()->json(['success' => 'Pomyślnie usunięto.']);
    }


    public function update($id, Request $request)
    {
        $borrower = Borrower::findOrFail($id);
        $rules = array(
            'name'      => ['required', 'string', 'max:255'],
            'post_code' => ['required'],
            'city'      => ['required'],
            'address'   => ['required']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }
        else
        {
            $borrower->name          = $request->name;
            $borrower->reg_number    = $request->reg_number;
            $borrower->nip           = $request->nip;
            $borrower->krs           = $request->krs;
            $borrower->post_code     = $request->post_code;
            $borrower->city          = $request->city;
            $borrower->address       = $request->address;
            $borrower->active        = $request->active;
            $borrower->save();

            return response()->json(['success' => 'Pomyślnie zapisano.']);
        }
    }
}
