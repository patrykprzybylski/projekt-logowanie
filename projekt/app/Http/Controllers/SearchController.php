<?php

namespace App\Http\Controllers;

use App\Borrower;
use Illuminate\Http\Request;
use View;
use Barryvdh\DomPDF\Facade as PDF;

class SearchController extends Controller
{
    public function index()
    {
        return view('search.index');
    }


    public function search(Request $request)
    {
        $borrower = Borrower::where('nip', $request->nip)->first();
        $loans = $borrower->loans;
        $loansAmount = $borrower->loans->sum('commitment_amount');
        $positiveLoans = $loans->where('rating' , '1');
        $negativeLoans = $loans->where('rating' ,'!=', '1');

        return response()->json(View::make('search.details', array('borrower' => $borrower, 'loans' => $loans, 'loansAmount' => $loansAmount,
            'positiveLoans' => $positiveLoans, 'negativeLoans' => $negativeLoans))->render());

    }


    public function generatePDF($nip)
    {
        $borrower = Borrower::where('nip', $nip)->first();
        $loans = $borrower->loans;
        $loansAmount = $borrower->loans->sum('commitment_amount');
        $positiveLoans = $loans->where('rating' , '1');
        $negativeLoans = $loans->where('rating' ,'!=', '1');

        $data = ['borrower' => $borrower, 'loans' => $loans, 'loansAmount' => $loansAmount, 'positiveLoans' => $positiveLoans, 'negativeLoans' => $negativeLoans];

        $pdf = PDF::loadView('pdf.raport', $data);

        return $pdf->download('Raport.pdf');
    }
}
