<?php

namespace App\Http\Controllers;

use App\Institution;
use App\User;
use Faker\Provider\File;
use http\Env\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use View;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $users = User::sortable(['id' => 'desc'])->paginate(10);
        $allUsersCount = count(User::all());

        if($request->get('records'))
        {
            $users = User::sortable()->paginate($request->get('records'));
        }
        if ($request->get('role'))
        {
            if ($request->get('active') === 'not' && $request->get('role'))
            {
                $users = User::sortable()->where('role', $request->get('role'))->where('active', 0)->paginate();
            }
            else if ($request->get('active') === 'active' && $request->get('role'))
            {
                $users = User::sortable()->where('role', $request->get('role'))->where('active', 1)->paginate();
            }
            else
            {
                $users = User::sortable()->where('role', $request->get('role'))->paginate();
            }
            return response()->json(View::make('usersList', array('users' => $users, 'countUsers' => count($users)))->render());
        }

        if ($request->get('active') === 'active')
        {
            if ($request->get('active') === 'active' && $request->get('role'))
            {
                $users = User::sortable()->where('role', $request->get('role'))->where('active', 1)->paginate();
            }
            else
            {
                $users = User::sortable()->where('active', 1)->paginate();
            }
            return response()->json(View::make('usersList', array('users' => $users, 'countUsers' => count($users)))->render());
        }

        if ($request->get('active') === 'not')
        {
            if ($request->get('active') === 'not' && $request->get('role'))
            {
                $users = User::sortable()->where('role', $request->get('role'))->where('active', 0)->paginate();
            }
            else
            {
                $users = User::where('active', 0)->paginate();
            }
            return response()->json(View::make('usersList', array('users' => $users, 'countUsers' => count($users)))->render());

        }

        if($request->get('userName'))
        {
            $users = User::sortable()->where('name', 'like', '%' . $request->get('userName') . '%')->orWhere('surname', 'like',  '%' . $request->get('userName') . '%')->paginate();
            return response()->json(View::make('usersList', array('users' => $users, 'countUsers' => count($users)))->render());
        }

        if($request->ajax())
        {
            return response()->json(View::make('usersList', array('users' => $users, 'countUsers' => count($users)))->render());
        }

        return view('index',  array('users' => $users, 'allUsers' => $allUsersCount));
    }


    public function create()
    {
        $institutions = Institution::all();

        return view('create')->with('institutions', $institutions);
    }


    public function store(Request $request)
    {
        $rules = array(
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }

        $form_data = array(
            'name'          => $request->name,
            'surname'       => $request->surname,
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
            'phone'         => $request->phone,
            'role'          => $request->role,
            'active'        => true
        );

        if($request->institution)
        {
            $institution = Institution::find($request->institution);
            $form_data['institution_id'] = $request->institution;
            $form_data['institution_name'] = $institution->name;
        }

        $user = User::create($form_data);

            $image = $request->file('file');
            if($image)
            {
                $new_name = 'avatar-' . $user->id . '.jpg';
                $image->move(public_path('images'), $new_name);
            }



        return response()->json(['success' => 'Pomyślnie dodano użytkownika.']);
    }


    public function edit($id)
    {
        $user = User::findOrFail($id);
        $institutions = Institution::all();

        return view('edit')->with('user', $user)->with('institutions', $institutions);
    }


    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $rules = array(
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }
        else
        {
            $user->name         = $request->name;
            $user->surname      = $request->surname;
            $user->email        = $request->email;
            $user->phone        = $request->phone;
            $user->role         = $request->role;
            $user->active       = $request->active;
            $user->institution_id  = $request->institution;
            if($request->institution){
                $institution = Institution::find($request->institution);
                $user->institution_name = $institution->name;
            }
            $user->save();

            return response()->json(['success' => 'Zapisano zmiany.']);
        }
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json(['success' => 'Pomyślnie usunięto użytkownika.']);
    }


    public function changePassword($id, Request $request)
    {
        $user = User::findOrFail($id);

        if(strcmp($request->get('new-password_confirmation'), $request->get('new-password')) == 0)
        {
            //Change Password
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect()->back()->with("success","Hasło zmienione!");

        }
        else {
            return redirect()->back()->with("error","Hasła nie są takie same.");
        }
    }


    public function uploadFile(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        if($validation->passes())
        {
            $image = $request->file('file');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();;
            $image->move(public_path('images'), $new_name);
            $user = User::findOrFail($request->user_id);
            $user->avatar = $new_name;
            $user->save();
            Artisan::call('cache:clear');
            return response()->json([
                'success'   => $new_name
            ]);
        }
        else
        {
            return response()->json([
                'errors'   => $validation->errors()->all()
            ]);
        }
    }


    public function deleteAvatar(Request $request)
    {
//        unlink(public_path('images/avatar-' . $request->id . '.jpg'));
        if(\File::exists(public_path('images/'.$request->avatar))){

            \File::delete(public_path('images/'.$request->avatar));
            $user = User::findOrFail($request->id);
            $user->avatar = null;
            $user->save();
            return response()->json([
                'success'   => 'Image Deleted Successfully'
            ]);

        }
        else
        {
            return response()->json([
                'error'   => $request->avatar
            ]);
        }

    }


    public function getUsers()
    {
       return datatables(User::all())->toJson();
    }
}
