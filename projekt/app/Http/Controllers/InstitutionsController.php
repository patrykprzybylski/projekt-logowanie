<?php

namespace App\Http\Controllers;

use App\Institution;
use App\User;
use Illuminate\Http\Request;
use Validator;
use View;


class InstitutionsController extends Controller
{
    public function index(Request $request)
    {
        $institutions = Institution::sortable(['id' => 'desc'])->paginate(10);

        if($request->get('records'))
        {
            $institutions = Institution::sortable()->paginate($request->get('records'));
        }

        if ($request->get('active') === 'active')
        {
            $institutions = Institution::sortable()->where('active', 1)->paginate(10);
            $countInstitutions = count(Institution::where('active', 1)->get());
            return response()->json(View::make('institutions.list', array('institutions' => $institutions, 'countInstitutions' => $countInstitutions))->render());
        }

        if ($request->get('active') === 'not')
        {
            $institutions = Institution::sortable()->where('active', 0)->paginate(10);
            $countInstitutions = count(Institution::where('active', 0)->get());
            return response()->json(View::make('institutions.list', array('institutions' => $institutions, 'countInstitutions' => $countInstitutions))->render());
        }

        if($request->get('searchVal'))
        {
            $institutions = Institution::sortable()->where('name', 'like', '%' . $request->get('searchVal') . '%')->orWhere('city', 'like',  '%' . $request->get('searchVal') . '%')->paginate(10);
            $countInstitutions = count(Institution::where('name', 'like', '%' . $request->get('searchVal') . '%')->orWhere('city', 'like',  '%' . $request->get('searchVal') . '%')->get());
            return response()->json(View::make('institutions.list', array('institutions' => $institutions, 'countInstitutions' => $countInstitutions))->render());
        }

        if($request->ajax())
        {
            return response()->json(View::make('institutions.list', array('institutions' => $institutions, 'countInstitutions' => count($institutions)))->render());
        }

        return view('institutions.index', array('institutions' => $institutions, 'allInstitutions' => count(Institution::all())));
    }


    public function store(Request $request)
    {
        $rules = array(
            'name'      => ['required', 'string', 'max:255'],
            'post_code' => ['required'],
            'city'      => ['required'],
            'address'   => ['required']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }

        $form_data = array(
            'name'          => $request->name,
            'reg_number'    => $request->reg_number,
            'nip'           => $request->nip,
            'krs'           => $request->krs,
            'post_code'     => $request->post_code,
            'city'          => $request->city,
            'address'       => $request->address,
            'active'        => $request->active
        );

        $institution = Institution::create($form_data);
        return response()->json(['institution' => $institution]);
    }


    public function destroy($id)
    {
        $institution = Institution::findOrFail($id);
        $institution->delete();

        return response()->json(['success' => 'Pomyślnie usunięto.']);
    }


    public function update($id, Request $request)
    {
        $institution = Institution::findOrFail($id);
        $rules = array(
            'name'      => ['required', 'string', 'max:255'],
            'post_code' => ['required'],
            'city'      => ['required'],
            'address'   => ['required']
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }
        else
        {
            $institution->name          = $request->name;
            $institution->reg_number    = $request->reg_number;
            $institution->nip           = $request->nip;
            $institution->krs           = $request->krs;
            $institution->post_code     = $request->post_code;
            $institution->city          = $request->city;
            $institution->address       = $request->address;
            $institution->active        = $request->active;
            $institution->save();

            return response()->json(['success' => 'Pomyślnie zapisano.']);
        }
    }
}
