<?php

namespace App\Http\Controllers;

require_once '../vendor/autoload.php';
use GusApi\BulkReportTypes;
use GusApi\Exception\InvalidUserKeyException;
use GusApi\Exception\NotFoundException;
use GusApi\GusApi;
use GusApi\ReportTypes;
use Illuminate\Http\Request;
use SoapClient;

class GusApiController extends Controller
{
    public function checkNip(Request $request)
    {
//        $gus = new GusApi('your api key here');
//for development server use:
        $gus = new GusApi('abcde12345abcde12345', 'dev');
        try {
            $nipToCheck = $request->nip; //change to valid nip value
            $gus->login();
            $gusReports = $gus->getByNip($nipToCheck);

            foreach ($gusReports as $gusReport) {
                //you can change report type to other one
                $reportType = ReportTypes::REPORT_ACTIVITY_PHYSIC_PERSON;
                $name =  $gusReport->getName();
                $address = $gusReport->getStreet().' '.$gusReport->getPropertyNumber().'/'.$gusReport->getApartmentNumber();
                $city = $gusReport->getCity();
                $postCode = $gusReport->getZipCode();
                $fullReport = $gus->getFullReport($gusReport, $reportType);

                return response()->json(['name' => $name, 'address' => $address, 'nip' => $request->nip, 'city' => $city, 'post_code' => $postCode]);
            }
        } catch (InvalidUserKeyException $e) {
            return response()->json('Bad user key');
        } catch (NotFoundException $e) {
            return response()->json($gus->getResultSearchMessage());
        }
    }
}
