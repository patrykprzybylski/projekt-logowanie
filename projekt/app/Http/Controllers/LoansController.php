<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\Institution;
use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use View;

class LoansController extends Controller
{
    public function index(Request $request)
    {
        if(Auth::user()->role == 1)
        {
            $institutions = Institution::all();
            $borrowers = Borrower::all();
            $loans = Loan::sortable(['id' => 'desc'])->paginate(10);
            $countLoans = count(Loan::all());
            $countLoansAmount = Loan::all()->sum('commitment_amount');
        }
        else
        {
            $institutionID = Auth::user()->institution_id;
            $institutions = Institution::where('id', $institutionID)->get();
            $borrowers = Borrower::all();
            $loans = Loan::where('institution', 'LIKE', Auth::user()->institution_name)->sortable(['id' => 'desc'])->paginate(10);
            $countLoans = count($loans);
            $countLoansAmount = Loan::where('institution','LIKE', Auth::user()->institution_name)->get()->sum('commitment_amount');
        }

        if($request->ajax())
        {

            return response()->json(View::make('loans.list', array('loans' => $loans, 'countLoans' => $countLoans, 'institutions' => $institutions, 'borrowers' => $borrowers, 'countLoansAmount' => $countLoansAmount))->render());
        }

        return view('loans.index', array('loans' => $loans, 'institutions' => $institutions, 'borrowers' => $borrowers, 'countLoans' => $countLoans, 'countLoansAmount' => $countLoansAmount));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'              => 'required',
            'institution'       => 'required',
            'borrower'          => 'required',
            'conclusion_date'   => 'required',
            'award_date'        => 'required',
            'period'            => 'required',
            'loan_amount'       => 'required',
            'commitment_amount' => 'required'
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }

        $borrower = Borrower::findOrFail($request->borrower);
        $form_data = array(
            'name'              => $request->name,
            'goal'              => $request->goal,
            'institution'       => $request->institution,
            'borrower'          => $borrower->name,
            'borrower_id'       => $request->borrower,
            'conclusion_date'   => date('Y-m-d', strtotime($request->conclusion_date)),
            'award_date'        => date('Y-m-d', strtotime($request->award_date)),
            'period'            => $request->period,
            'loan_amount'       => $request->loan_amount,
            'commitment_amount' => $request->commitment_amount,
            'de_minimis'        => $request->de_minimis,
            'status'            => $request->status,
            'payoff_date'       =>  date('Y-m-d', strtotime($request->payoff_date)),
            'rating'            => $request->rating
        );

        Loan::create($form_data);
        return response()->json(['success' => 'Pomyślnie dodano.']);
    }


    public function destroy($id)
    {
        $loan = Loan::findOrFail($id);
        $loan->delete();

        return response()->json(['success' => 'Pomyślnie usunięto.']);
    }


    public function getInstitutionDetails($institutionName)
    {
        $institution = Institution::where('name', 'like', $institutionName)->first();

        return response()->json(['institution' => $institution]);
    }


    public function getBorrowerDetails($borrowerName)
    {
        $borrower = Borrower::where('name', 'like', $borrowerName)->first();

        return response()->json(['borrower' => $borrower]);
    }


    public function filter(Request $request, Loan $loan)
    {
        $loan = $loan->newQuery();
        $institutions = Institution::all();
        $borrowers = Borrower::all();
        if (isset($request->institution)) {
            $loan->where('institution', $request->input('institution'));
        }

        if (isset($request->borrower)) {
            $loan->where('borrower', $request->input('borrower'));
        }

        if (isset($request->status)) {
            $loan->where('status', $request->input('status'));
        }

        if (isset($request->rating)) {
            $loan->where('rating', $request->input('rating'));
        }

        if(isset($request->created_from) && isset($request->created_to))
        {
            $from = date('Y-m-d', strtotime($request->created_from));
            $to = date('Y-m-d', strtotime($request->created_to));
            $loan->whereBetween('created_at', [$from, $to]);
        }

        if(isset($request->created_from))
        {
           $loan->where('created_at', '>=',  date('Y-m-d', strtotime($request->created_from)));
        }

        if(isset($request->created_to))
        {
            $loan->where('created_at', '<=',  date('Y-m-d', strtotime($request->created_to)));
        }

        if(isset($request->searchVal))
        {
            $loan->where('borrower', 'like', '%' . $request->get('searchVal') . '%')
            ->orWhere('commitment_amount', 'like', '%' . $request->get('searchVal') . '%')
            ->orWhere('institution', 'like', '%' . $request->get('searchVal') . '%');
        }

        $countLoansAmount = $loan->get()->sum('commitment_amount');

        return response()->json(View::make('loans.list', array('loans' => $loan->sortable(['id' => 'desc'])->paginate(10),'institutions' => $institutions, 'borrowers' => $borrowers, 'countLoansAmount' => $countLoansAmount, 'countLoans' => count($loan->get())))->render());
    }


    public function update($id, Request $request)
    {
        $loan = Loan::findOrFail($id);
        $rules = array(
            'name'              => 'required',
            'institution'       => 'required',
            'borrower'          => 'required',
            'conclusion_date'   => 'required',
            'award_date'        => 'required',
            'period'            => 'required',
            'loan_amount'       => 'required',
            'commitment_amount' => 'required'
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->getMessageBag()->toArray()]);
        }
        else
        {
            $loan->name              = $request->name;
            $loan->goal              = $request->goal;
            $loan->institution       = $request->institution;
            $loan->borrower          = $request->borrower;
            $loan->conclusion_date   = date('Y-m-d', strtotime($request->conclusion_date));
            $loan->award_date        = date('Y-m-d', strtotime($request->award_date));
            $loan->period            = $request->period;
            $loan->loan_amount       = $request->loan_amount;
            $loan->commitment_amount = $request->commitment_amount;
            $loan->de_minimis        = $request->de_minimis;
            $loan->status            = $request->status;
            $loan->payoff_date       = date('Y-m-d', strtotime($request->payoff_date));
            $loan->rating            = $request->rating;
            $loan->save();

            return response()->json(['success' => 'Pomyślnie zapisano.']);
        }
    }

}
