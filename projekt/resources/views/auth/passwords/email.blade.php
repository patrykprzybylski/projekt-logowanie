@extends('layouts.app')

@section('content')
    @if (session('status'))--}}
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

                    <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                        <div class="kt-login__wrapper">
                            <div class="kt-login__container">
                                <div class="kt-login__body">
                                    <div class="kt-login__logo">
                                        <a href="{{ url('/') }}">
                                            <img src="{{ asset('images/logo-2.png') }}">
                                        </a>
                                    </div>
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">Przypomnij hasło</h3>
                                        <div class="kt-login__desc">Podaj adres e-mail w celu przypomnienia hasła:</div>
                                    </div>
                                    <div class="kt-login__form">
                                        <form class="kt-form"  method="POST" action="{{ route('password.email') }}">
                                            @csrf
                                            <div class="form-group">
                                                <input class="form-control @error('email') is-invalid @enderror" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off" required>
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Wyślij</button>
                                                <a href="{{ url('/login') }}"><button id="kt_login_forgot_cancel" type="button" class="btn btn-outline-brand btn-pill">Anuluj</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url({{ asset('images/bg-4.jpg') }});">
                    <div class="kt-login__section">
                        <div class="kt-login__block">
                            <h3 class="kt-login__title">Biuro Informacji Pożyczkowej</h3>
                            <div class="kt-login__desc">
                                                                Lorem ipsum dolor sit amet, coectetuer adipiscing
                                                                <br>elit sed diam nonummy et nibh euismod
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            @if(count($errors) > 0)
            @foreach($errors->all() as $error)
            toastr.error("Wprowadzono zły adres email.");
            @endforeach
            @endif
        });
    </script>
@endsection
