@extends('layouts.app')

@section('content')

    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__body">
                                <div class="kt-login__logo">
                                    <a href="{{ url('/') }}">
                                        <img src="{{ asset('images/logo-2.png') }}">
                                    </a>
                                </div>
                                <div class="kt-login__signin">
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">Zaloguj się</h3>
                                    </div>
                                    <div class="kt-login__form">
                                        <form class="kt-form"  method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group">
                                                <input class="form-control @error('email') is-invalid @enderror" type="text" placeholder="Email" name="email" autocomplete="off" required>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control form-control-last @error('password') is-invalid @enderror" type="password" placeholder="Hasło" name="password" required>
                                            </div>
                                            <div class="kt-login__extra">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="remember"> Zapamiętaj mnie
                                                    <span></span>
                                                </label>
                                                <a href="{{ route('password.request') }}" id="kt_login_forgot">Nie pamiętasz hasła ?</a>
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">Zaloguj</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>


{{--                                <div class="kt-login__forgot">--}}
{{--                                    <div class="kt-login__head">--}}
{{--                                        <h3 class="kt-login__title">Forgotten Password ?</h3>--}}
{{--                                        <div class="kt-login__desc">Enter your email to reset your password:</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="kt-login__form">--}}
{{--                                        <form class="kt-form" action="">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">--}}
{{--                                            </div>--}}
{{--                                            <div class="kt-login__actions">--}}
{{--                                                <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Request</button>--}}
{{--                                                <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>--}}
{{--                                            </div>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url({{ asset('images/bg-4.jpg') }});">
                    <div class="kt-login__section">
                        <div class="kt-login__block">
                            <h3 class="kt-login__title">Biuro Informacji Pożyczkowej</h3>
                            <div class="kt-login__desc">
                                Lorem ipsum dolor sit amet, coectetuer adipiscing
                                <br>elit sed diam nonummy et nibh euismod
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            @if(count($errors) > 0)
            @foreach($errors->all() as $error)
            toastr.error("Wprowadzono zły adres email lub hasło.");
            @endforeach
            @endif
        });
    </script>

@endsection
