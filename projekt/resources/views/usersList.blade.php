<table class="kt-datatable__table" id="@isset($countUsers){{ $countUsers }}@endisset">
    <thead class="kt-datatable__head">
    <tr class="kt-datatable__row">
        <th style="width:5%"  class="kt-datatable__cell lp"><span>LP</span></th>
        <th style="width:33%" class="kt-datatable__cell"><span>@sortablelink('name', 'Nazwa', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width:26%" class="kt-datatable__cell"><span>@sortablelink('phone', 'Telefon', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width:16%" class="kt-datatable__cell"><span>@sortablelink('role', 'Rola', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width:10%" class="kt-datatable__cell"><span>@sortablelink('active', 'Aktywność', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="max-width: 110px;" class="kt-datatable__cell action-table"><span>Akcje</span></th>
    </tr>
    </thead>
    <tbody class="kt-datatable__body">
    @foreach($users as $user)
        <tr class="kt-datatable__row">
            <td style="width:5%" class="kt-datatable__cell"><span>{{ ($users ->currentpage()-1) * $users ->perpage() + $loop->index + 1 }}</span></td>
            <td style="width:33%" class="kt-datatable__cell">
                <div class="row">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__pic">
                            @if($user->avatar)
                                <div style="width:40px;">
                                    <img style="max-width: 40px !important; height: 40px !important; object-fit: cover !important;" alt="avatar" src="{{ asset('images/' . $user->avatar . '') }}">
                                </div>
                            @else
                                <div style="width:40px;">
                                    <img alt="avatar" src="{{ asset('avatar.png') }}">
                                </div>
                            @endif
                        </div>
                        <div class="kt-user-card-v2__details" style="line-height: 1.5 !important;">
                            <span>{{ $user->name }} {{ $user->surname }}</span>
                        </div>
                    </div>
                </div>
            </td>
            <td style="width:26%" class="kt-datatable__cell"><span>{{ $user->phone }}</span> </td>
            <td style="width:16%" class="kt-datatable__cell"><span>
                    @if($user->role == 1) Administrator @else Fundusz @endif
                </span> </td>
            <td style="width:10%" class="kt-datatable__cell text-center">
               @if($user->active)
                   <i class="fa fa-check fa-2x kt-font-success"></i>
               @else
                    <i class="fa fa-close fa-2x kt-font-danger"></i>
                @endif
            </td>
            <td style="max-width: 110px;" class="kt-datatable__cell action-table">
                <span>
                    <a data-toggle="tooltip" data-placement="top" title="Edytuj" href="{{ url('/edit/' . $user->id) }}" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-edit"></i></a>
                    <span data-toggle="modal" data-target="#deleteModal{{ $user->id }}"><a data-toggle="tooltip" data-placement="top" title="Usuń" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-trash"></i></a></span>
                </span>
            </td>
        </tr>

        <!-- Modal -->
        <div class="modal fade" id="deleteModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Usuwanie użytkownika</h3>
                    </div>
                    <div class="modal-body">
                        <p>Czy na pewno chcesz usunąć użytkownika?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        <button type="button" class="btn btn-danger btn-destroy-user" id="{{ $user->id }}">Usuń</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </tbody>
</table>

@if(count($users) <= 0)
    <div class="text-center mt-3"><i class="fa fa-warning fa-3x mr-3"></i><p>Brak rekordów</p></div>
@endif

<div class="kt-datatable__pager kt-datatable--paging-loaded">
    {{ $users->appends(\Request::except('page'))->links('layouts.pagination') }}

    <div class="justify-content-center">
        <form action="/" method="GET" class="">
            <div class="kt-pagination  kt-pagination--brand">
                <div class="kt-pagination__toolbar">
                    <span class="pagination__desc mr-2">Rekordów</span>
                    <select name="records" class="form-control kt-font-brand records" style="width: 60px;">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.btn-destroy-user').on('click', function(e){
            $('.kt-spinner').css('display', 'block');
            $('.btn-destroy-user').css('display', 'none');
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: 'destroy/' + id,
                method: 'POST',
                data: {
                    '_token' : $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data)
                {
                    $('.kt-spinner').css('display', 'none');
                    $('.btn-destroy-user').css('display', 'block');
                    location.reload();
                }

            });
        });


        // $('body').on('change', '.records', function(e){
        //     console.log('change');
        //     e.preventDefault();
        //     $.ajax({
        //         url : '/',
        //         dataType: 'json',
        //         contentType: 'application/json',
        //         data: {
        //             'records'      : $( ".records" ).val(),
        //         }
        //     }).done(function (data){
        //         $('.kt-datatable').html(data);
        //     });
        // });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<style>
    @media screen and (max-width: 1200px) {
        .action-table{
            width: 15% !important;
        }
    }

    .active-sort {
        color: #5d78ff !important;
    }

    .sorting {
        color: #595d6e;
    }

</style>
