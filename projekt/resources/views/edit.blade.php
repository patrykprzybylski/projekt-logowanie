@extends('layouts.main')

@section('content')

    @if (session('success'))
        <div class="">
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        </div>
    @endif

    @if (session('error'))
        <div class="">
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        </div>
    @endif

    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Użytkownicy</h3>
                    <div class="kt-subheader__breadcrumbs">
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ url('/edit/'.$user->id) }}" class="kt-subheader__breadcrumbs-link">
                            Edycja użytkownika
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_apps_contacts_view_tab_2" role="tab" aria-selected="true">
                                        <i class="flaticon2-calendar-3"></i> Informacje podstawowe
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_3" role="tab" aria-selected="false">
                                        <i class="flaticon2-gear"></i> Ustawienia
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content  kt-margin-t-20">
                            <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                                <div class="kt-section">
                                    <div class="kt-section__body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Informacje podstawowe:</h3>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">Zdjęcie:</label>
                                            <div class="col-lg-6">

                                                <div class="kt-avatar kt-avatar--outline" id="kt_apps_user_add_avatar">
                                                    @if($user->avatar)
                                                        <div class="kt-avatar__holder avatar-div" style="background-size: cover !important; background-image: url({{ asset('/images/'. $user->avatar .'') }})"></div>
                                                    @else
                                                        <div class="kt-avatar__holder avatar-div" style="background-size: cover !important; background-image: url({{ asset('avatar.png') }})"></div>
                                                    @endif
                                                    <form method="post" id="upload_form" enctype="multipart/form-data">
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Zmień zdjęcie">
                                                        <i class="fa fa-edit"></i>
                                                        <input type="file" id="select_file" name="select_file" accept=".png, .jpg, .jpeg">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                           <i class="fa fa-times"></i>
                                                        </span>
                                                    </form>
                                                </div>

                                                <div>
                                                    @if($user->avatar)
                                                        <i data-toggle="kt-tooltip" title="" data-original-title="Usuń zdjęcie" class="fa fa-trash delete-avatar" style="margin-left: 53px; cursor: pointer;"></i>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <form method="put" id="store-form" class="kt-form kt-form--label-right">
                                    @csrf
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"><strong>Imię:</strong></label>
                                                    <div class="col-lg-6">
                                                        <input value="{{ $user->name }}" type="text" class="form-control" name="name" id="name">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"><strong>Nazwisko:</strong></label>
                                                    <div class="col-lg-6">
                                                        <input value="{{ $user->surname }}" type="text" class="form-control" name="surname" id="surname">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Telefon:</label>
                                                    <div class="col-lg-6">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                            <input value="{{ $user->phone }}" type="text" class="form-control" name="phone" id="phone">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"><strong>E-mail:</strong></label>
                                                    <div class="col-lg-6">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                                            <input value="{{ $user->email }}" type="email" class="form-control" name="email" id="email">
                                                        </div>
                                                        <p>Adres e-mail służy do logowania się w systemie</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg">
                                        </div>

                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Rola:</label>
                                                    <div class="col-lg-6">
                                                        <label class="btn btn-label-brand btn-bold btn-sm kt-margin-t-5 kt-margin-b-5 role-label">@if($user->role == 1) Administrator @else Fundusz @endif</label>
                                                        @if($user->institution_name && $user->role == 2)
                                                            <p class="mt-1 institution-p">{{ $user->institution_name }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Aktywność konta:</label>
                                                    <div class="col-lg-6">
                                                        @if($user->active == 1)
                                                            <label class="btn btn-label-success btn-bold btn-sm kt-margin-t-5 kt-margin-b-5 active-label"> Aktywne </label>
                                                        @else
                                                            <label class="btn btn-label-danger btn-bold btn-sm kt-margin-t-5 kt-margin-b-5 active-label"> Nieaktywne </label>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg">
                                        </div>

                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                <div class="row">
                                                    <div class="col-lg-3">

                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                                                        <button type="submit" class="btn btn-brand btn-bold add-btn">Zapisz</button>
                                                        <a href="{{ url('/') }}" class="btn btn-clean btn-bold">Powrót</a>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </form>
                            </div>

                            <div class="tab-pane" id="kt_apps_contacts_view_tab_3" role="tabpanel">
                                <form method="put" id="store-form2" class="kt-form kt-form--label-right">
                                @csrf
                                <div class="kt-section">
                                    <div class="kt-section__body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Dostęp</h3>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">Aktywność konta:</label>
                                            <div class="col-lg-6">
                                                <span class="kt-switch">
                                                <label>
                                                    <input type="checkbox" id="active" @if($user->active) checked="checked" @endif name="active" value="{{ $user->active }}">
                                                    <span class="active-switch"></span>
                                                </label>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">Hasło:</label>
                                            <div class="col-lg-6">
                                                <button type="button" data-toggle="modal" data-target="#change-password" class="btn btn-label-brand btn-bold btn-sm kt-margin-t-5 kt-margin-b-5">Zmień hasło</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg">
                                    </div>

                                    @if(Auth::user()->role == 1)
                                    <div class="kt-section__body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Rola</h3>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <div class="kt-radio-list">
                                                            <label class="kt-radio">
                                                                <input type="radio" @if($user->role == 1)checked="checked" @endif name="role" value="1" class="role-input"> Administrator
                                                                <span></span>
                                                            </label>
                                                            <label class="kt-radio">
                                                                <input type="radio" @if($user->role == 2)checked="checked" @endif value="2" name="role" class="role-input"> Fundusz
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 mt-1 institutions-container" style="display: none">
                                                        <select data-live-search="true" name="institutions" id="institutions" class="form-control">
                                                            @if($user->institution_id)
                                                                <option value="{{$user->institution_id}}">{{ $user->institution_name }}</option>
                                                            @else
                                                                <option value="">Wybierz</option>
                                                            @endif
                                                            @foreach($institutions as $institution)
                                                                <option value="{{$institution->id}}">{{ $institution->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg">
                                    </div>

                                    <div class="kt-section__body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-6">
                                                <h3 class="kt-section__title kt-section__title-sm">Informacje dodatkowe:</h3>
                                                <span><strong>Dodano:</strong></span><br/>
                                                <span>{{ Auth::user()->name }} {{ Auth::user()->surname }}</span><br>
                                                <span>{{ $user->created_at->format('Y-m-d') }} o godzinie ({{ $user->created_at->format('H:m') }})</span><br/><br/>

                                                <span><strong>Zmodyfikowano:</strong></span><br/>
                                                <span>{{ Auth::user()->name }} {{ Auth::user()->surname }}</span><br>
                                                <span>{{ $user->updated_at->format('Y-m-d') }} o godzinie ({{ $user->updated_at->format('H:m') }})</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg">
                                    </div>
                                    @endif

                                    <div class="kt-section__body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-6">
                                                <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                                                <button type="submit" class="btn btn-brand btn-bold add-btn">Zapisz</button>
                                                <a href="{{ url('/') }}" class="btn btn-clean btn-bold">Powrót</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


{{--    <div class="col-lg-8">--}}
{{--        <div class="kt-portlet">--}}
{{--            <div class="kt-portlet__head">--}}
{{--                <div class="kt-portlet__head-label">--}}
{{--                    <h3 class="kt-portlet__head-title">Formularz użytkownika</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <form method="put" id="store-form" class="kt-form">--}}
{{--                @csrf--}}
{{--                <div class="kt-portlet__body">--}}
{{--                    <div class="kt-section kt-section--first">--}}
{{--                        <div class="form-group row">--}}
{{--                            <label class="col-lg-3 col-form-label"><strong>Imię:</strong></label>--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <input value="{{ $user->name }}" type="text" class="form-control" name="name" id="name">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label class="col-lg-3 col-form-label"><strong>Nazwisko:</strong></label>--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <input value="{{ $user->surname }}" type="text" class="form-control" name="surname" id="surname">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label class="col-lg-3 col-form-label"><strong>E-mail:</strong></label>--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <input value="{{ $user->email }}" type="email" class="form-control" name="email" id="email">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label class="col-lg-3 col-form-label">Telefon:</label>--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <input value="{{ $user->phone }}" type="text" class="form-control" name="phone" id="phone">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-9 text-right">--}}
{{--                            <button type="button" data-toggle="modal" data-target="#change-password" class="btn btn-primary">Zmień hasło</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="kt-portlet__foot">--}}
{{--                    <div class="form-group row">--}}
{{--                        <label class="col-lg-3 col-form-label">Rola:</label>--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="kt-radio-list">--}}
{{--                                <label class="kt-radio">--}}
{{--                                    <input type="radio" @if($user->role == 1)checked="checked" @endif name="role" value="1" class="role-input"> Administrator--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                                <label class="kt-radio">--}}
{{--                                    <input type="radio" @if($user->role == 2)checked="checked" @endif value="2" name="role" class="role-input"> Użytkownik--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="kt-portlet__foot">--}}
{{--                    <div class="kt-form__actions">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>--}}
{{--                                <button type="submit" class="btn btn-success add-btn">Zapisz</button>--}}
{{--                                <a href="{{ url('/') }}" class="btn btn-clean btn-icon-sm">Powrót</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="col-lg-4">--}}
{{--        <div class="kt-portlet">--}}
{{--            <div class="kt-portlet__head">--}}
{{--                <div class="kt-portlet__head-label">--}}
{{--                    <h3 class="kt-portlet__head-title">Zdjęcie</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="kt-portlet__body">--}}
{{--                <div class="kt-widget__media text-center">--}}
{{--                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle">--}}
{{--                        @if(file_exists('images/avatar-' . $user->id . '.jpg'))--}}
{{--                            <div class="kt-avatar__holder" style="background-image: url({{ asset('/images/avatar-'. $user->id .'.jpg') }})"></div>--}}
{{--                        @else--}}
{{--                            <div class="kt-avatar__holder" style="background-image: url({{ asset('avatar.png') }})"></div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="kt-portlet__foot">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-7">--}}
{{--                        <div class="custom-file">--}}
{{--                            <form method="post" id="upload_form" enctype="multipart/form-data">--}}
{{--                            <input type="file" class="custom-file-input" id="select_file" name="select_file">--}}
{{--                            <label class="custom-file-label" for="select_file">Wybierz z</label>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-5 align-self-center">--}}
{{--                        <button type="submit" class="btn btn-secondary delete-avatar"><i class="fa fa-trash fa-2x"></i></button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="kt-portlet">--}}
{{--            <div class="kt-portlet__head">--}}
{{--                <div class="kt-portlet__head-label">--}}
{{--                    <h3 class="kt-portlet__head-title">Informacje</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="kt-portlet__body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-6 text-right">--}}
{{--                        <p>Data dodania: </p>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <span>{{ $user->created_at }}</span> <br/>--}}
{{--                        <span>{{ Auth::user()->name }} {{ Auth::user()->surname }}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-6 text-right">--}}
{{--                        <p>Data modyfikacji:</p>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <span>{{ $user->updated_at }}</span> <br/>--}}
{{--                        <span>{{ Auth::user()->name }} {{ Auth::user()->surname }}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="kt-portlet__foot">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-6 text-right align-self-center">--}}
{{--                        <p>Aktywność:</p>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <span class="kt-switch">--}}
{{--                        <label>--}}
{{--                            <input type="checkbox" id="active" @if($user->active) checked="checked" @endif name="active" value="{{ $user->active }}">--}}
{{--                            <span class="active-switch"></span>--}}
{{--                        </label>--}}
{{--                        </span>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


    <!-- Modal -->
    <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ url('changePassword/' . $user->id) }}">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Zmień hasło</h3>
                </div>
                <div class="modal-body">
{{--                   <div class="form-group row">--}}
{{--                       <label class="col-lg-4 col-form-label">Hasło:</label>--}}
{{--                       <div class="col-lg-8">--}}
{{--                           <input type="password" class="form-control" name="current-password" id="current-password" required>--}}
{{--                       </div>--}}
{{--                   </div>--}}

                   <div class="form-group row">
                       <label class="col-lg-4 col-form-label">Nowe hasło:</label>
                       <div class="col-lg-8">
                           <input type="password" class="form-control" name="new-password" id="new-password" required>
                       </div>
                   </div>

                   <div class="form-group row">
                       <label class="col-lg-4 col-form-label">Powtórz nowe hasło:</label>
                       <div class="col-lg-8">
                           <input type="password" class="form-control" name="new-password_confirmation" id="new-password_confirmation" required>
                       </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-brand btn-bold">Zmień</button>
                    <button type="button" class="btn btn-clean btn-bold" data-dismiss="modal">Zamknij</button>
                </div>
                </form>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function(){
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            showInstitutions();
            $('.kt-radio').on('click', function(){
                showInstitutions();
            });

            $('#institutions').selectpicker();

            $('#store-form, #store-form2').on('submit', function(e) {
                e.preventDefault();
                $('input').removeClass('is-invalid');
                $('.invalid-feedback').remove();
                $('.kt-spinner').css('display', 'block');
                $('.add-btn').hide();

                $.ajax({
                    url: '/edit/{{ $user->id }}',
                    method: 'POST',
                    data: {
                        '_token' : $('meta[name="csrf-token"]').attr('content'),
                        'name' : $('#name').val(),
                        'surname' : $('#surname').val(),
                        'email' : $('#email').val(),
                        'phone' : $('#phone').val(),
                        'role'  : $('.role-input:checked').val(),
                        'active' : $('#active').val(),
                        'institution' : $('#institutions').val()
                    },
                    success:function(data)
                    {
                        if(data.success)
                        {
                            toastr.success("Pomyślnie zapisano zmiany.");
                            changeRole();
                            changeActiveStatus();
                        }
                        if(data.errors)
                        {
                            toastr.error("Wypełnij poprawnie wszystkie pola.");
                            $.each(data.errors, function(key, value){
                                $('input[name=' + key + ']').addClass('is-invalid').after('<div class="invalid-feedback">' + value + '</div>');
                            });
                        }

                        $('.kt-spinner').css('display', 'none');
                        $('.add-btn').show();

                    }
                });
            });


            $("input:file").change(function (e){
                e.preventDefault();
                var file_data = $('#select_file').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('user_id', {{ $user->id }});

                $.ajax({
                    url:"{{ route('ajaxupload.uploadFile') }}",
                    method:"POST",
                    data: form_data,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data)
                    {
                        if(data.success)
                        {
                            console.log(data.success);
                            var url = '/images/' + data.success;
                            $('.avatar-div').css('background-image', 'url("'+url+'")');
                        }
                    }
                });
            });


            $('.delete-avatar').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('ajaxdelete') }}",
                    method: "POST",
                    data: {
                        'avatar' : '{{ $user->avatar }}',
                        'id'     : '{{ $user->id }}'
                    },
                    success: function(data)
                    {
                        $('.avatar-div').css('background-image', 'url("/avatar.png")');
                        toastr.success("Pomyślnie usunięto zdjęcie.");
                    }
                });
            });

            $('.active-switch').on('click', function(){
                $('#active').val() == '1' ?  $('#active').val('0') :  $('#active').val('1');
            });

        });

        $(function () {
            $('[data-toggle="kt-tooltip"]').tooltip()
        });


        function changeRole()
        {
            let val = $('.role-input:checked').val();
            if(val === '1')
            {
                $('.role-label').html('Administrator');
                $('.institution-p').css('display', 'none');
            }
            if(val === '2')
            {
                $('.role-label').html('Fundusz');
                $('.institution-p').css('display', 'block');
            }
        }


        function showInstitutions()
        {
                let val = $('.role-input:checked').val();
                if(val === '1')
                {
                    $('.institutions-container').css('display', 'none');
                }
                if(val === '2')
                {
                    $('.institutions-container').css('display', 'block');
                }
        }


        function changeActiveStatus()
        {
            let val = $('#active').val();
            if(val === '1')
            {
                $('.active-label').html('Aktywne').removeClass('btn-label-danger').addClass('btn-label-success');
            }
            if(val === '0')
            {
                $('.active-label').html('Nieaktywne').removeClass('btn-label-success').addClass('btn-label-danger');
            }
        }
    </script>
@endsection
