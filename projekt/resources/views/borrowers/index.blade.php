@extends('layouts.main')

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Pożyczkobiorcy</h3>
                    <div class="kt-subheader__breadcrumbs">
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ url('/borrowers') }}" class="kt-subheader__breadcrumbs-link">
                            Lista pożyczkobiorców
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Filtrowanie
                                </h3>
                                <div class="show-filters ml-3">
                                    <span style="display: none;" class="btn btn-label-primary fraza-btn">Fraza<i style="cursor: pointer;" class="fa fa-close ml-1 remove-fraza"></i></span>
                                </div>
                            </div>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a class="btn btn-sm btn-icon btn-clean btn-icon-md" data-toggle="collapse" href="#filters"><i class="fa fa-angle-down"></i></a>
                                </div>
                            </div>
                        </div>

                        <form action="/borrowers" method="GET" class="collapse" id="filters">
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="active">Aktywność</label>
                                            <select name="active" class="form-control" id="active">
                                                <option value="">Wszystkie</option>
                                                <option value="active">Aktywne</option>
                                                <option value="not">Nieaktywne</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <button type="submit" class="btn btn-primary filter-institutions">Filtruj</button>
                                <button class="btn btn-secondary reset-filters">Wyczyść</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Lista pożyczkobiorców </h3><span class="ml-2">(</span><span class="count">{{ $allBorrowers }}</span>/<span>{{ $allBorrowers }}</span>)
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <button data-toggle="modal" data-target="#add_borrower" class="btn btn-brand btn-icon-sm"><i class="fa fa-plus fa-2x"></i>Dodaj pożyczkobiorcę</button>
                            </div>
                        </div>

                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm kt-margin-t-20">
                            <div class="form-group kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <form action="/" method="GET" class="">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control user-name" placeholder="Szukaj..." id="generalSearch" name="searchVal">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="fa fa-search"></i></span>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded kt-margin-t-20">
                            @include('borrowers.list')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



{{--add borrower modal--}}
    <div class="modal fade" id="add_borrower" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pożyczkobiorca</h5>
                </div>
                <form method="post" id="store-form" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="name" id="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Numer wpisu do rejestru</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="reg_number" id="reg_number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">NIP</label>
                            <div class="col-3">
                                <input class="form-control" type="number" name="nip" id="nip">
                            </div>
                            <div class="col-5">
                                <a data-toggle="modal" data-target="#add_from_gus" class="btn btn-secondary">Pobierz z GUS</a>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">KRS</label>
                            <div class="col-8">
                                <input class="form-control" type="number" name="krs" id="krs">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Kod i miejscowość</strong></label>
                            <div class="col-3">
                                <input class="form-control" type="text" required name="post_code" id="post_code">
                            </div>
                            <div class="col-5">
                                <input class="form-control" type="text" required name="city" id="city">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Adres</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="address" id="address">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Aktywność</label>
                            <div class="col-8">
                        <span class="kt-switch">
                        <label>
                            <input type="checkbox" id="active-input" checked="checked" name="active" value="1">
                            <span class="active-switch"></span>
                        </label>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary add-btn">Dodaj</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>



{{--GUS modal --}}
    <div class="modal fade" id="add_from_gus" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pobierz dane z GUS</h5>
                </div>
                <form method="post" id="store-from-gus" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>NIP</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="gus-nip" id="gus-nip">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button type="submit" class="btn btn-primary get-nip get-nip-edit">Pobierz</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            getActives();

            $('.active-switch').on('click', function(){
                $('#active-input').val() == '1' ?  $('#active-input').val('0') :  $('#active-input').val('1');
            });

            $('body').on('click', '.edit-active-switch', function(){
                var id = $(this).attr('id').replace('switch', '');
                $('#active-input'+id).val() == '1' ?  $('#active-input'+id).val('0') :  $('#active-input'+id).val('1');
            });

            $('.add-btn').on('click', function(e){
                e.preventDefault();
                $('input').removeClass('is-invalid');
                $('.invalid-feedback').remove();
                $('.kt-spinner').css('display', 'block');
                $('.add-btn').css('display', 'none');

                $.ajax({
                    url: '/borrowers',
                    method: 'POST',
                    data: {
                        '_token' : $('meta[name="csrf-token"]').attr('content'),
                        'name' : $('#name').val(),
                        'reg_number' : $('#reg_number').val(),
                        'nip' : $('#nip').val(),
                        'krs' :  $('#krs').val(),
                        'post_code' : $('#post_code').val(),
                        'city' : $("#city").val(),
                        'address' : $("#address").val(),
                        'active' : $("#active-input").val(),
                    },
                    success:function(data)
                    {
                        console.log(data);
                        if(data.borrower)
                        {
                            $('#add_borrower').modal('hide');
                            //sweet alert
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Gratulacje',
                                text: 'Pożyczkobiorca został dodany',
                                showConfirmButton: true
                            })
                        }
                        if(data.errors)
                        {
                            toastr.error("Nazwa jest wymagana<br>Kod i miejscowość jest wymagana<br>Adres jest wymagany");
                            $.each(data.errors, function(key, value){
                                // $('input[name=' + key + ']').addClass('is-invalid').after('<div class="invalid-feedback">' + value + '</div>');
                                $('input[name=' + key + ']').addClass('is-invalid');
                            });
                        }

                        $('.kt-spinner').css('display', 'none');
                        $('.add-btn').css('display', 'inline');

                    }
                });
            });


            $('.filter-institutions').on('click', function(e){
                e.preventDefault();
                $.ajax({
                    url : '/borrowers',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'active'    : $( "#active" ).val(),
                    }
                }).done(function (data) {

                    $('.kt-datatable').html(data);
                    //show filters
                    $('.show-filters').html('');
                    if($( "#active" ).val())
                    {
                        var val = $('#active option:selected').text();
                        $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Aktywność<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active"></i></span>')
                    }

                    countBorrowers();
                }).fail(function () {
                    alert('Nie można wczytać.');
                });
            });


            $('.reset-filters').on('click', function(e){
                e.preventDefault();
                $( "#active" ).val('');
                $.ajax({
                    url : '/borrowers',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (data) {
                    $('.kt-datatable').html(data);
                    $('.show-filters').html('');
                    countBorrowers();
                }).fail(function () {
                    alert('Nie można wczytać.');
                });
            });


            $('.user-name').on('keyup', function(e){
                e.preventDefault();
                $.ajax({
                    url : '/borrowers',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'searchVal'  : $( ".user-name" ).val(),
                    }
                }).done(function (data) {
                    $('.kt-datatable').html(data);
                    $('.fraza-btn').css('display', 'inline');
                    countBorrowers();
                }).fail(function () {
                    alert('Nie można wczytać uzytkowników.');
                });
            });



            $('.get-nip').on('click', function(e){
                e.preventDefault();
                $('input').removeClass('is-invalid');
                $.ajax({
                    url : '/gus',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'nip'    : $( "#gus-nip" ).val(),
                    },
                }).done(function (data) {
                    if(data.name)
                    {
                        $('#add_from_gus').modal('hide');
                        $('#name').val(data.name);
                        $('#nip').val(data.nip);
                        $('#post_code').val(data.post_code);
                        $('#city').val(data.city);
                        $('#address').val(data.address);
                    }
                    else {
                        $( "#gus-nip" ).addClass('is-invalid');
                        toastr.error('Nie można znaleźć pożyczkobiorcy z takim numerem nip.');
                    }

                }).fail(function (data) {
                    console.log(data);
                });
            });
        });


        $('body').on('click', '.remove-active', function(e){
            $( "#active" ).val('');
            $('.active-btn').remove();
            $.ajax({
                url : '/borrowers',
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                countBorrowers();
            });
        });


        $('body').on('click', '.remove-fraza', function(e){
            $( ".user-name" ).val('');
            e.preventDefault();
            $.ajax({
                url : '/borrowers',
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                $('.fraza-btn').css('display', 'none');
                countBorrowers();
            }).fail(function () {
                alert('Nie można wczytać.');
            });
        });


        $('body').on('click', '.sorting', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
            });
        });


        $('body').on('click', '.pag-link', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
            });
        });


        function getActives()
        {
            $.ajax({
                url : '/borrowers',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'active'    : 'active',
                }
            }).done(function (data) {

                $('.kt-datatable').html(data);
                $( "#active" ).val('active');
                //show filters
                if($( "#active" ).val())
                {
                    var val = $('#active option:selected').text();
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Aktywność<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active"></i></span>')
                }

                countBorrowers();
            }).fail(function () {
                alert('Nie można wczytać.');
            });
        }


        function countBorrowers()
        {
            var nr = $('table').attr('id');
            if(nr)
            {
                $('.count').html(nr);
            }
        }
    </script>
@endsection
