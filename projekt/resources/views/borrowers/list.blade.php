<table class="kt-datatable__table" id="@isset($countBorrowers){{ $countBorrowers }}@endisset">
    <thead class="kt-datatable__head">
    <tr class="kt-datatable__row">
        <th style="width:5%"  class="kt-datatable__cell lp"><span>LP</span></th>
        <th style="width: 35%;"><span>@sortablelink('name', 'Nazwa', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 20%;"><span>@sortablelink('city', 'Miejscowość', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 20%;"><span>@sortablelink('nip', 'NIP', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 10%"><span>@sortablelink('active', 'Aktywność', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 120px !important;" class="kt-datatable__cell action-table"><span>Akcje</span></th>
    </tr>
    </thead>
    <tbody class="kt-datatable__body">
    @foreach($borrowers as $borrower)
        <tr class="kt-datatable__row">
            <td style="width:5%" class="kt-datatable__cell"><span>{{ ($borrowers ->currentpage()-1) * $borrowers ->perpage() + $loop->index + 1 }}</span></td>
            <td style="width: 35%;">{{ $borrower->name }}</td>
            <td style="width: 20%;">{{ $borrower->city }}</td>
            <td style="width: 20%;">{{ $borrower->nip }}</td>
            <td class="text-center"  style="width: 10%">
                @if($borrower->active)
                    <i class="fa fa-check fa-2x kt-font-success"></i>
                @else
                    <i class="fa fa-close fa-2x kt-font-danger"></i>
                @endif
            </td>
            <td class="kt-datatable__cell action-table"  style="width: 120px !important;">
                <span>
                    <span data-toggle="modal" data-target="#edit{{ $borrower->id }}"><a data-toggle="tooltip" data-placement="top" title="Edytuj" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-edit"></i></a></span>
                    <span data-toggle="modal" data-target="#deleteModal{{ $borrower->id }}"><a data-toggle="tooltip" data-placement="top" title="Usuń" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-trash"></i></a></span>
                </span>
            </td>
        </tr>

        {{-- Edit modal--}}
        <div class="modal fade" id="edit{{ $borrower->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pożyczkobiorca</h5>
                    </div>
                    <form method="post" class="kt-form update-borrowers-form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa</strong></label>
                                <div class="col-8">
                                    <input class="form-control" type="text" required name="name_edit{{ $borrower->id }}" value="{{ $borrower->name }}" id="name-edit{{ $borrower->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Numer wpisu do rejestru</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="reg_number_edit{{ $borrower->id }}"  value="{{ $borrower->reg_number }}" id="reg_number-edit{{ $borrower->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">NIP</label>
                                <div class="col-3">
                                    <input class="form-control" type="number" name="nip_edit{{ $borrower->id }}" id="nip-edit{{ $borrower->id }}" value="{{ $borrower->nip }}">
                                </div>
                                <div class="col-5">
                                    <a data-toggle="modal" data-target="#add_from_gus{{ $borrower->id }}" class="btn btn-secondary">Pobierz z GUS</a>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">KRS</label>
                                <div class="col-8">
                                    <input class="form-control" type="number" name="krs_edit{{ $borrower->id }}" id="krs-edit{{ $borrower->id }}" value="{{ $borrower->krs }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Kod i miejscowość</strong></label>
                                <div class="col-3">
                                    <input class="form-control" type="text" required name="post_code_edit{{ $borrower->id }}" id="post_code-edit{{ $borrower->id }}" value="{{ $borrower->post_code }}">
                                </div>
                                <div class="col-5">
                                    <input class="form-control" type="text" required name="city_edit{{ $borrower->id }}" id="city-edit{{ $borrower->id }}" value="{{ $borrower->city }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Adres</strong></label>
                                <div class="col-8">
                                    <input class="form-control" type="text" required name="address_edit{{ $borrower->id }}" id="address-edit{{ $borrower->id }}" value="{{ $borrower->address }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Aktywność</label>
                                <div class="col-8">
                        <span class="kt-switch">
                        <label>
                            <input type="checkbox" id="active-input{{ $borrower->id }}" @if($borrower->active) checked="checked" @endif  name="active_edit{{ $borrower->id }}" value="{{ $borrower->active }}">
                            <span class="edit-active-switch" id="switch{{ $borrower->id }}"></span>
                        </label>
                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-left">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                            <button class="btn btn-primary update-borrower" id="update-{{ $borrower->id }}">Zapisz</button>
                            <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal to delete-->
        <div class="modal fade" id="deleteModal{{ $borrower->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Usuwanie pożyczkobiorcy</h3>
                    </div>
                    <div class="modal-body">
                        <p>Czy na pewno chcesz usunąć pożyczkobiorcę?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        <button type="button" class="btn btn-danger btn-destroy-user" id="{{ $borrower->id }}">Usuń</button>
                    </div>
                </div>
            </div>
        </div>


        {{--GUS modal --}}
        <div class="modal fade" id="add_from_gus{{$borrower->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pobierz dane z GUS</h5>
                    </div>
                    <form method="post" id="store-from-gus" class="kt-form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>NIP</strong></label>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="gus-nip" id="gus-nip{{ $borrower->id }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-left">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                            <button type="submit" class="btn btn-primary get-nip-edit" id="gus{{ $borrower->id }}">Pobierz</button>
                            <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    </tbody>
</table>

@if(count($borrowers) <= 0)
    <div class="text-center mt-3"><i class="fa fa-warning fa-3x mr-3"></i><p>Brak rekordów</p></div>
@endif

<div class="kt-datatable__pager kt-datatable--paging-loaded">
    {{ $borrowers->appends(\Request::except('page'))->links('layouts.pagination') }}

    <div class="justify-content-center">
        <form action="/" method="GET" class="">
            <div class="kt-pagination  kt-pagination--brand">
                <div class="kt-pagination__toolbar">
                    <span class="pagination__desc mr-2">Rekordów</span>
                    <select name="records" class="form-control kt-font-brand records" style="width: 60px;">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$(function () {
$('[data-toggle="tooltip"]').tooltip()
});

$(document).ready(function(){
    $('.btn-destroy-user').on('click', function(e){
        $('.kt-spinner').css('display', 'block');
        $('.btn-destroy-user').css('display', 'none');
        var id = $(this).attr('id');
        e.preventDefault();
        $.ajax({
            url: 'destroy-borrower/' + id,
            method: 'POST',
            data: {
                '_token' : $('meta[name="csrf-token"]').attr('content')
            },
            success:function(data)
            {
                $('.kt-spinner').css('display', 'none');
                $('.btn-destroy-user').css('display', 'block');
                location.reload();
            }
        });
    });
});


$('body').on('change', '.records', function(e){
    e.preventDefault();
    $.ajax({
        url : '/borrowers',
        dataType: 'json',
        contentType: 'application/json',
        data: {
            'records'      : $( ".records" ).val(),
        }
    }).done(function (data){
        $('.kt-datatable').html(data);
    });
});


$('body').on('click', '.update-borrower', function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr('id').replace('update-', '');
    $('input').removeClass('is-invalid');
    $('.invalid-feedback').remove();
    $('.kt-spinner').css('display', 'block');
    $('.add-btn').hide();
    console.log(id);

    $.ajax({
        url: '/edit-borrower/'+id,
        method: 'POST',
        data: {
            '_token' : $('meta[name="csrf-token"]').attr('content'),
            'name' : $('#name-edit' + id + '').val(),
            'reg_number' : $('#reg_number-edit' + id + '').val(),
            'nip' : $('#nip-edit' + id + '').val(),
            'krs' :  $('#krs-edit' + id + '').val(),
            'post_code' : $('#post_code-edit' + id + '').val(),
            'city' : $('#city-edit' + id + '').val(),
            'address' : $('#address-edit' + id + '').val(),
            'active' :  $('#active-input'+id).val()
        },
        success:function(data)
        {
            if(data.errors)
            {
                toastr.error("Wypełnij poprawnie wszystkie pola.");
                $.each(data.errors, function(key, value){
                    console.log(value);
                    $('input[name=' + key + ']').addClass('is-invalid').after('<div class="invalid-feedback">' + value + '</div>');
                });
            }
            else {
                toastr.success("Pomyślnie zapisano zmiany.");
                setTimeout(function(){ location.reload(); }, 1000);
            }

            $('.kt-spinner').css('display', 'none');
            $('.add-btn').show();
        }
    });
});

$('body').on('click', '.get-nip-edit', function(e){
   e.preventDefault();
   var id = $(this).attr('id').replace('gus','');
    $('input').removeClass('is-invalid');
    $.ajax({
        url : '/gus',
        dataType: 'json',
        contentType: 'application/json',
        data: {
            'nip'    : $( "#gus-nip"+id ).val(),
        },
    }).done(function (data) {
        if(data.name)
        {
            $('#add_from_gus'+id).modal('hide');
            $('#name-edit'+id).val(data.name);
            $('#nip-edit'+id).val(data.nip);
            $('#post_code-edit'+id).val(data.post_code);
            $('#city-edit'+id).val(data.city);
            $('#address-edit'+id).val(data.address);
        }
        else {
            $( "#gus-nip"+id ).addClass('is-invalid');
            toastr.error('Nie można znaleźć pożyczkobiorcy z takim numerem nip.');
        }

    }).fail(function (data) {
        console.log(data);
    });
});

</script>

<style>
@media screen and (max-width: 1200px) {
.action-table{
        width: 120px; !important;
    }
}

.active-sort {
    color: #5d78ff !important;
}

.sorting {
    color: #595d6e;
    font-weight: 500;
}
</style>