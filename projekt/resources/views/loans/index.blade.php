@extends('layouts.main')

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Pożyczki</h3>
                    <div class="kt-subheader__breadcrumbs">
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ url('/loans') }}" class="kt-subheader__breadcrumbs-link">
                            Lista pożyczek
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Filtrowanie
                                </h3>
                                <div class="show-filters ml-3">
                                    <span style="display: none;" class="btn btn-label-primary fraza-btn">Fraza<i style="cursor: pointer;" class="fa fa-close ml-1 remove-fraza"></i></span>
                                </div>
                            </div>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a class="btn btn-sm btn-icon btn-clean btn-icon-md" data-toggle="collapse" href="#filters"><i class="fa fa-angle-down"></i></a>
                                </div>
                            </div>
                        </div>

                        <form action="/loans" method="GET" class="collapse" id="filters">
                            <div class="kt-portlet__body">
                                <div class="row">

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Data dodania</label>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <input class="form-control from date-sort">
                                                </div>
                                                <div class="col-lg-6">
                                                    <input class="form-control to date-sort">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Fundusz</label>
                                            @if(Auth::user()->role == 1)
                                            <select data-live-search="true" name="institutions-sort" class="form-control institution-1" id="institutions-sort">
                                                <option value="">Wybierz</option>
                                                @foreach($institutions as $institution)
                                                    <option value="{{$institution->name}}">{{ $institution->name }}</option>
                                                @endforeach
                                            </select>
                                            @else
                                                <select data-live-search="true" name="institutions-sort" class="form-control" id="institutions-sort">
                                                    @foreach($institutions as $institution)
                                                        <option value="{{$institution->name}}">{{ $institution->name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Klient (Pożyczkobiorca)</label>
                                            <select data-live-search="true" name="borrowers-sort" class="form-control" id="borrowers-sort">
                                                <option value="">Wybierz</option>
                                                @foreach($borrowers as $borrower)
                                                    <option value="{{$borrower->name}}">{{ $borrower->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status-sort" class="form-control" id="status-sort">
                                                <option value="">Wybierz</option>
                                                <option value="1">Aktywna</option>
                                                <option value="2">Zakończona</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Ocena</label>
                                            <select name="rating-sort" class="form-control" id="rating-sort">
                                                <option value="">Wybierz</option>
                                                <option value="1">Spłata bez opóźnień</option>
                                                <option value="2">Opóźnienie w spłacie do 30 dni</option>
                                                <option value="3">Opóźnienie w spłacie od 30 do 90 dni</option>
                                                <option value="4">Opóźnienie w spłacie powyżej 90 dni</option>
                                                <option value="5">Windykacja (zobowiązanie odzyskane)</option>
                                                <option value="6">Windykacja (zobowiązanie nieodzyskane)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <button type="submit" class="btn btn-primary filter-loans">Filtruj</button>
                                <button class="btn btn-secondary reset-filters">Wyczyść</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{--   podsumowanie --}}
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Podsumowanie
                                </h3>
                            </div>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a class="btn btn-sm btn-icon btn-clean btn-icon-md" data-toggle="collapse" href="#summary"><i class="fa fa-angle-down"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="summary" class="collapse">
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-3 text-center">
                                        <p>Ilość pożyczek</p>
                                        <h4 class="loans-sum">{{ $countLoans }}</h4>
                                    </div>
                                    <div class="col-3 text-center">
                                        <p>Wartość pożyczek</p>
                                        <h4 class="amount-sum">{{ $countLoansAmount }} zł</h4>
                                    </div>
                                    <div class="col-3 text-center">
                                        <p>Aktywne</p>
                                        <h4 class="active-sum">
                                            @if(Auth::user()->role == 1)
                                            {{ count(\App\Loan::all()->where('status', 1)) }}
                                            @else
                                            {{ count(\App\Loan::where('institution', Auth::user()->institution_name)->where('status', 1)->get()) }}
                                            @endif
                                        </h4>
                                    </div>
                                    <div class="col-3 text-center">
                                        <p>Zakończone</p>
                                        <h4 class="end-sum">
                                            @if(Auth::user()->role == 1)
                                            {{ count(\App\Loan::all()->where('status', 2)) }}
                                            @else
                                            {{ count(\App\Loan::where('institution', Auth::user()->institution_name)->where('status', 2)->get()) }}
                                            @endif
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Lista pożyczek </h3><span class="ml-2">(</span><span class="count">{{ $countLoans }}</span>/<span>{{ $countLoans }}</span>)
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <button data-toggle="modal" data-target="#add_loan" class="btn btn-brand btn-icon-sm"><i class="fa fa-plus fa-2x"></i>Dodaj pożyczkę</button>
                            </div>
                        </div>

                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm kt-margin-t-20">
                            <div class="form-group kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <form action="/" method="GET" class="d-flex flex-row align-items-center">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control loan-name" placeholder="Szukaj..." id="generalSearch" name="searchVal">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="fa fa-search"></i></span>
                                    </span>
                                    </div>

                                    <label data-click-state="1" class="kt-checkbox ml-2 details-label" style="margin-bottom: 0 !important;">
                                        <input type="checkbox" checked class="details-checkbox" value="0"> Szczegóły
                                        <span></span>
                                    </label>
                                </form>
                            </div>
                        </div>
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded kt-margin-t-20">
                            @include('loans.list')
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


{{-- add loan modal--}}
    <div class="modal fade" id="add_loan" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dodawanie pożyczki</h5>
                </div>
                <form method="post" id="store-form" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa projektu</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="name" id="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Cel finansowania</label>
                            <div class="col-8">
                                <textarea class="form-control" type="text" rows="4" name="goal" id="goal"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Fundusz pożyczkowy</strong></label>
                            <div class="col-6">
                                <select data-live-search="true" name="institutions" id="institutions" class="form-control">
                                    @if(Auth::user()->role == 1)
                                    <option value="">Wybierz</option>
                                    @endif
                                    @foreach($institutions as $institution)
                                        <option value="{{$institution->name}}">{{ $institution->name }}</option>
                                    @endforeach
                                </select>
                                <span class="institution-details">
                                </span>
                            </div>
                            @if(Auth::user()->role == 1)
                            <div class="col-2">
                                <span data-toggle="tooltip" data-placement="top" title="Dodaj fundusz pożyczkowy" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i data-toggle="modal" data-target="#add_institution" class="fa fa-plus fa-2x"></i></span>
                            </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Pożyczkobiorca</strong></label>
                            <div class="col-6">
                                <select data-live-search="true" name="borrowers" id="borrowers" class="form-control">
                                    <option value="">Wybierz</option>
                                    @foreach($borrowers as $borrower)
                                        <option value="{{$borrower->id}}">{{ $borrower->name }}</option>
                                    @endforeach
                                </select>
                                <span class="borrower-details">
                                </span>
                            </div>
                            <div class="col-2">
                                <span data-toggle="tooltip" data-placement="top" title="Dodaj pożyczkobiorcę" class="btn  btn-clean btn-icon btn-icon-md"><i data-toggle="modal" data-target="#add_borrower" class="fa fa-plus fa-2x"></i></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Data zawarcia umowy</strong></label>
                            <div class="col-2">
                                <input class="form-control date-input" type="text" name="conclusion_date" id="conclusion_date">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Data udzielonej pożyczki</strong></label>
                            <div class="col-2">
                                <input class="form-control date-input" type="text" name="award_date" id="award_date">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Okres</strong></label>
                            <div class="col-3 input-group">
                                <input class="form-control" type="text" name="period" id="period">
                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">mc</span></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Kwota pożyczki</strong></label>
                            <div class="col-3 input-group">
                                <input class="form-control" type="text" name="loan_amount" id="loan_amount">
                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Kwota zobowiązania</strong></label>
                            <div class="col-3 input-group">
                                <input class="form-control" type="text" name="commitment_amount" id="commitment_amount">
                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"></label>
                            <div class="col-8">
                                <label class="kt-checkbox de-minimis-label">
                                    <input type="checkbox" class="de-minimis-checkbox"> Czy udzielona jest pomocą de miminis
                                    <span></span>
                                </label>
                                <textarea style="display:none" class="form-control" rows="4" type="text" name="de_minimis" id="de_minimis"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Status</label>
                            <div class="col-5">
                                <select class="form-control" name="stats" id="status">
                                    <option data-content="<span class='btn btn-label-success btn-bold btn-sm p-0'>Aktywny</span>" value="1">Aktywny</option>
                                    <option data-content="<span class='btn btn-label-brand btn-bold btn-sm p-0'> Zakończony </label>" value="2">Zakończony</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Data spłaty</label>
                            <div class="col-2">
                                <input class="form-control date-input" type="text" name="payoff_date" id="payoff_date">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Ocena</label>
                            <div class="col-8">
                                <select name="rating" class="form-control" id="rating">
                                    <option value="">Wybierz</option>
                                    <option data-icon="fa fa-2x fa-thumbs-up kt-font-success" value="1">Spłata bez opóźnień</option>
                                    <option  data-icon="fa fa-2x fa-thumbs-down kt-font-warning" value="2">Opóźnienie w spłacie do 30 dni</option>
                                    <option  data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="3">Opóźnienie w spłacie od 30 do 90 dni</option>
                                    <option  data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="4">Opóźnienie w spłacie powyżej 90 dni</option>
                                    <option  data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="5">Windykacja (zobowiązanie odzyskane)</option>
                                    <option  data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="6">Windykacja (zobowiązanie nieodzyskane)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary add-btn">Dodaj</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{--  Add institution modal  --}}
    <div class="modal fade" id="add_institution" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Instytucja pożyczkowa</h5>
                </div>
                <form method="post" id="store-form" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="name" id="institution-name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Numer wpisu do rejestru</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="reg_number" id="reg_number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">NIP</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="nip" id="nip">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">KRS</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="krs" id="krs">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Kod i miejscowość</strong></label>
                            <div class="col-3">
                                <input class="form-control" type="text" required name="post_code" id="post_code">
                            </div>
                            <div class="col-5">
                                <input class="form-control" type="text" required name="city" id="city">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Adres</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="address" id="address">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Aktywność</label>
                            <div class="col-8">
                        <span class="kt-switch">
                        <label>
                            <input type="checkbox" id="active-input" checked="checked" name="active" value="1">
                            <span class="active-switch"></span>
                        </label>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary add-institution">Dodaj</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>


{{-- Add borrower modal--}}
    <div class="modal fade" id="add_borrower" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pożyczkobiorca</h5>
                </div>
                <form method="post" id="store-form" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="borrower-name" id="borrower-name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Numer wpisu do rejestru</label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="borrower-reg_number" id="borrower-reg_number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">NIP</label>
                            <div class="col-3">
                                <input class="form-control" type="number" name="borrower-nip" id="borrower-nip">
                            </div>
                            <div class="col-5">
                                <a data-toggle="modal" data-target="#add_from_gus" class="btn btn-secondary">Pobierz z GUS</a>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">KRS</label>
                            <div class="col-8">
                                <input class="form-control" type="number" name="borrower-krs" id="borrower-krs">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Kod i miejscowość</strong></label>
                            <div class="col-3">
                                <input class="form-control" type="text" required name="borrower-post_code" id="borrower-post_code">
                            </div>
                            <div class="col-5">
                                <input class="form-control" type="text" required name="borrower-city" id="borrower-city">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>Adres</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" required name="borrower-address" id="borrower-address">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right">Aktywność</label>
                            <div class="col-8">
                        <span class="kt-switch">
                        <label>
                            <input type="checkbox" id="borrower-active-input" checked="checked" name="active" value="1">
                            <span class="active-switch"></span>
                        </label>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-primary add-borrower">Dodaj</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{--GUS modal --}}
    <div class="modal fade" id="add_from_gus" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pobierz dane z GUS</h5>
                </div>
                <form method="post" id="store-from-gus" class="kt-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label text-right"><strong>NIP</strong></label>
                            <div class="col-8">
                                <input class="form-control" type="text" name="gus-nip" id="gus-nip">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button type="submit" class="btn btn-primary get-nip get-nip-edit">Pobierz</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $('.date-input').datepicker({
            format: 'dd-mm-yyyy',
            language: "pl",
            autoclose: true,
            container: '#add_loan'
        });

        $('.date-sort').datepicker({
            format: 'dd-mm-yyyy',
            language: "pl",
            autoclose: true,
        });



        $('#institutions, #borrowers, #institutions-sort, #borrowers-sort, #rating, #status').selectpicker();


        $('.add-btn').on('click', function(e){
            e.preventDefault();
            $('input').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            $('.kt-spinner').css('display', 'block');
            $('.add-btn').css('display', 'none');

            $.ajax({
                url: '/loans',
                method: 'POST',
                data: {
                    '_token'            : $('meta[name="csrf-token"]').attr('content'),
                    'name'              : $('#name').val(),
                    'goal'              : $('#goal').val(),
                    'institution'       : $('#institutions').val(),
                    'borrower'          : $('#borrowers').val(),
                    'conclusion_date'   : $('#conclusion_date').val(),
                    'award_date'        : $('#award_date').val(),
                    'period'            : $("#period").val(),
                    'loan_amount'       : $("#loan_amount").val(),
                    'commitment_amount' : $("#commitment_amount").val(),
                    'de_minimis'        : $('#de_minimis').val(),
                    'status'            : $('#status').val(),
                    'payoff_date'       : $('#payoff_date').val(),
                    'rating'            : $('#rating').val()
                },
                success:function(data)
                {
                    if(data.success)
                    {
                        $('#add_loan').modal('hide');
                        //clear inputs
                        $('.form-control').val('');
                        //sweet alert
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Gratulacje',
                            text: 'Pożyczka została dodana.',
                            showConfirmButton: true
                        });
                        setTimeout(function(){ location.reload(); }, 2000);
                    }
                    if(data.errors)
                    {
                        toastr.error("Wypełnij wszystkie wymagane pola.");
                        $.each(data.errors, function(key, value){
                            $('input[name=' + key + ']').addClass('is-invalid');
                        });
                    }

                    $('.kt-spinner').css('display', 'none');
                    $('.add-btn').css('display', 'inline');

                }
            });
        });


        $('.add-institution').on('click', function(e){
            e.preventDefault();
            $('input').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            $('.kt-spinner').css('display', 'block');
            $('.add-institution').css('display', 'none');

            $.ajax({
                url: '/institutions',
                method: 'POST',
                data: {
                    '_token' : $('meta[name="csrf-token"]').attr('content'),
                    'name' : $('#institution-name').val(),
                    'reg_number' : $('#reg_number').val(),
                    'nip' : $('#nip').val(),
                    'krs' :  $('#krs').val(),
                    'post_code' : $('#post_code').val(),
                    'city' : $("#city").val(),
                    'address' : $("#address").val(),
                    'active' : $("#active-input").val(),
                },
                success:function(data)
                {
                    if(data.institution)
                    {
                        $('#add_institution').modal('hide');
                        $("#institutions").append('<option value="'+data.institution.name+'">'+data.institution.name+'</option>');
                        $('#institutions').val(data.institution.name);
                        $("#institutions").selectpicker("refresh");
                        //actualize details
                        $('.institution-details').html('NIP: ' + data.institution.nip + ', ' + data.institution.city);
                    }
                    if(data.errors)
                    {
                        toastr.error("Nazwa jest wymagana<br>Kod i miejscowość jest wymagana<br>Adres jest wymagany");
                        $.each(data.errors, function(key, value){
                            $('input[name=' + key + ']').addClass('is-invalid');
                        });
                    }

                    $('.kt-spinner').css('display', 'none');
                    $('.add-institution').css('display', 'inline');

                }
            });
        });


        $('.add-borrower').on('click', function(e){
            e.preventDefault();
            $('input').removeClass('is-invalid');
            $('.invalid-feedback').remove();
            $('.kt-spinner').css('display', 'block');
            $('.add-borrower').css('display', 'none');

            $.ajax({
                url: '/borrowers',
                method: 'POST',
                data: {
                    '_token' : $('meta[name="csrf-token"]').attr('content'),
                    'name' : $('#borrower-name').val(),
                    'reg_number' : $('#borrower-reg_number').val(),
                    'nip' : $('#borrower-nip').val(),
                    'krs' :  $('#borrower-krs').val(),
                    'post_code' : $('#borrower-post_code').val(),
                    'city' : $("#borrower-city").val(),
                    'address' : $("#borrower-address").val(),
                    'active' : $("#borrower-active-input").val(),
                },
                success:function(data)
                {
                    if(data.borrower)
                    {
                        $('#add_borrower').modal('hide');
                        $("#borrowers").append('<option value="'+data.borrower.name+'">'+data.borrower.name+'</option>');
                        $('#borrowers').val(data.borrower.name);
                        $("#borrowers").selectpicker("refresh");
                        //actualize borrowers details
                        $('.borrower-details').html('NIP: ' + data.borrower.nip + ', ' + data.borrower.city);
                    }
                    if(data.errors)
                    {
                        toastr.error("Nazwa jest wymagana<br>Kod i miejscowość jest wymagana<br>Adres jest wymagany");
                        $.each(data.errors, function(key, value){
                            // $('input[name=' + key + ']').addClass('is-invalid').after('<div class="invalid-feedback">' + value + '</div>');
                            $('input[name=' + key + ']').addClass('is-invalid');
                        });
                    }

                    $('.kt-spinner').css('display', 'none');
                    $('.add-borrower').css('display', 'inline');

                }
            });
        });


        $('.get-nip').on('click', function(e){
            e.preventDefault();
            $('input').removeClass('is-invalid');
            $.ajax({
                url : '/gus',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'nip'    : $( "#gus-nip" ).val(),
                },
            }).done(function (data) {
                if(data.name)
                {
                    $('#add_from_gus').modal('hide');
                    $('#borrower-name').val(data.name);
                    $('#borrower-nip').val(data.nip);
                    $('#borrower-post_code').val(data.post_code);
                    $('#borrower-city').val(data.city);
                    $('#borrower-address').val(data.address);
                }
                else {
                    $( "#gus-nip" ).addClass('is-invalid');
                    toastr.error('Nie można znaleźć pożyczkobiorcy z takim numerem nip.');
                }

            }).fail(function (data) {
            });
        });


        $('#institutions').on('change', function(e){
            let selectVal = $(this).val();
            $.ajax({
                url : '/institution-details/' + selectVal,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                if(data.institution)
                {
                    $('.institution-details').html('NIP: ' + data.institution.nip + ', ' + data.institution.city);
                }
            });
        });


        $('#borrowers').on('change', function(e){
            let selectVal = $(this).val();
            $.ajax({
                url : '/borrower-details/' + selectVal,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                if(data.borrower)
                {
                    $('.borrower-details').html('NIP: ' + data.borrower.nip + ', ' + data.borrower.city);
                }
            });
        });


        $('.filter-loans').on('click', function(e){
            e.preventDefault();
            $('.show-filters').html('');
            $.ajax({
                url : '/filter-loans',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'institution'   : $('#institutions-sort').val(),
                    'borrower'      : $('#borrowers-sort').val(),
                    'status'        : $('#status-sort').val(),
                    'rating'        : $('#rating-sort').val(),
                    'created_from'  : $('.from').val(),
                    'created_to'    : $('.to').val()
                }
            }).done(function (data) {
                $('.kt-datatable').html(data);
                countLoans();
                actualizeSummary();
                if($( ".institution-1" ).val())
                {
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Fundusz<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active" id="institutions-remove"></i></span>')
                }
                if($( "#borrowers-sort" ).val())
                {
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Klient<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active" id="borrower-remove"></i></span>')
                }
                if($( "#status-sort" ).val())
                {
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Status<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active" id="status-remove"></i></span>')
                }
                if($( "#rating-sort" ).val())
                {
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Ocena<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active" id="rating-remove"></i></span>')
                }
                if($( ".from" ).val() || $( ".to" ).val())
                {
                    $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Data dodania<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active" id="date-remove"></i></span>')
                }
            });
        });


        $('.loan-name').on('keyup', function(e){
            e.preventDefault();
            $.ajax({
                url : '/filter-loans',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'institution'   : $('#institutions-sort').val(),
                    'borrower'      : $('#borrowers-sort').val(),
                    'status'        : $('#status-sort').val(),
                    'rating'        : $('#rating-sort').val(),
                    'created_from'  : $('.from').val(),
                    'created_to'    : $('.to').val(),
                    'searchVal'     : $( ".loan-name" ).val(),
                }
            }).done(function (data) {
                $('.kt-datatable').html(data);
                $('.fraza-btn').css('display', 'inline');
                countLoans();
            }).fail(function () {
                alert('Nie można wczytać.');
            });
        });


        $('.reset-filters').on('click', function(e){
            e.preventDefault();
            $( "select" ).val('');
            $('.institution-1, #borrowers-sort, #status-sort, #rating-sort').selectpicker('refresh');
            $('.from, .to').val('');
            $.ajax({
                url : '/loans',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function (data) {
                $('.kt-datatable').html(data);
                $('.show-filters').html('');
                countLoans();
                actualizeSummary();
            }).fail(function () {
                alert('Nie można wczytać.');
            });
        });


        $('.details-label').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            if($(this).attr('data-click-state') == '1') {
                $(this).attr('data-click-state', '0');
                $('.institution-table-info').css('display', 'none');
                $('.details-checkbox').prop('checked', false);
            } else {
                $(this).attr('data-click-state', '1');
                $('.institution-table-info').css('display', 'block');
                $('.details-checkbox').prop('checked', true);
            }
        });


        $('.de-minimis-label').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            if($(this).attr('data-click-state') == '1') {
                $(this).attr('data-click-state', '0');
                $('#de_minimis').css('display', 'none');
                $('.de-minimis-checkbox').prop('checked', false);
            } else {
                $(this).attr('data-click-state', '1');
                $('#de_minimis').css('display', 'block');
                $('.de-minimis-checkbox').prop('checked', true);
            }
        });


        $('body').on('click', '.sorting', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
            });
        });


        $('body').on('click', '.pag-link', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                showDetails();
            });
        });


        $('body').on('click', '.remove-fraza', function(e){
            $( ".loan-name" ).val('');
            e.preventDefault();
            $.ajax({
                url : '/loans',
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                $('.fraza-btn').css('display', 'none');
                countLoans();
            }).fail(function () {
                alert('Nie można wczytać.');
            });
        });


        $('body').on('click', '.remove-active', function(e){
            let what = $(this).attr('id').replace('-remove', '-sort');
            $( "#"+what ).val('');
            $(this).parent().remove();
            $.ajax({
                url : '/filter-loans',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'institution'   : $('#institutions-sort').val(),
                    'borrower'      : $('#borrowers-sort').val(),
                    'status'        : $('#status-sort').val(),
                    'rating'        : $('#rating-sort').val(),
                    'created_from'  : $('.from').val(),
                    'created_to'    : $('.to').val()
                }
            }).done(function (data) {
                $('.kt-datatable').html(data);
                countLoans();
            });
        });


        function countLoans()
        {
            var nr = $('table').attr('id');
            if(nr)
            {
                $('.count').html(nr);
            }
        }


        function showDetails()
        {
            if($('.details-label').attr('data-click-state') == '0') {
                $('.institution-table-info').css('display', 'none');
            } else {
                $('.institution-table-info').css('display', 'block');
            }
        }

        function actualizeSummary()
        {
            let amount = $('thead').attr('id');
            $('.amount-sum').html(amount + ' zł');

            var nr = $('table').attr('id');
            if(nr)
            {
                $('.loans-sum').html(nr);
            }
        }
    </script>
@endsection
