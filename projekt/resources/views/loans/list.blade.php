<table class="kt-datatable__table" id="@isset($countLoans){{ $countLoans }}@endisset">
    <thead id="@isset($countLoansAmount) {{ $countLoansAmount }}@endisset" class="kt-datatable__head">
    <tr class="kt-datatable__row">
        <th style="width:5%;"  class="kt-datatable__cell lp"><span>LP</span></th>
        <th style="width: 10%;"  class="kt-datatable__cell"><span>Dodano</span></th>
        <th style="width: 35%;"><span>@sortablelink('name', 'Klient/Fundusz', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 10%;"><span>@sortablelink('commitment_amount', 'Kwota', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 10%;"><span>@sortablelink('period', 'Okres', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 10%;"><span>@sortablelink('status', 'Status', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 10%;"><span>@sortablelink('rating', 'Ocena', ['filter' => 'active, visible'], ['class' => 'sorting'])</span></th>
        <th style="width: 120px !important;" class="kt-datatable__cell action-table"><span>Akcje</span></th>
    </tr>
    </thead>
    <tbody class="kt-datatable__body">
    @foreach($loans as $loan)
        <tr class="kt-datatable__row">
            <td style="width:5%;" class="kt-datatable__cell"><span>{{ ($loans ->currentpage()-1) * $loans ->perpage() + $loop->index + 1 }}</span></td>
            <td style="width: 10%;">{{ $loan->created_at->format('d-m-Y') }}</td>
            <td style="width: 35%;"><strong>{{ $loan->borrower }}</strong> <br/> <span class="institution-table-info">{{ $loan->institution }}</span></td>
            <td style="width: 10%;" class="text-center"><strong>{{ $loan->commitment_amount }} zł</strong></td>
            <td style="width: 10%;" class="text-center">{{ $loan->period }}</td>
            <td style="width: 10%;">
                @if($loan->status == 1)
                    <label class="btn btn-label-success btn-bold btn-sm kt-margin-t-5 kt-margin-b-5 role-label"> Aktywny </label>
                @elseif($loan->status == 2)
                    <label class="btn btn-label-brand btn-bold btn-sm kt-margin-t-5 kt-margin-b-5 active-label"> Zakończony </label>
                @endif
            </td>
            <td style="width: 10%;" class="text-center">
                @if($loan->rating && $loan->rating == 1)
                    <i data-toggle="tooltip" data-placement="top" title="Spłata bez opóźnień" class="fa fa-2x fa-thumbs-up kt-font-success"></i>
                @elseif($loan->rating && $loan->rating == 2)
                    <i data-toggle="tooltip" data-placement="top" title="Opóźnienie w spłacie do 30 dni" class="fa fa-2x fa-thumbs-down kt-font-warning"></i>
                @else
                    <i data-toggle="tooltip" data-placement="top"
                    title="
                    @if($loan->rating == 3) Opóźnienie w spłacie od 30 do 90 dni
                     @elseif($loan->rating == 4) Opóźnienie w spłacie powyżej 90 dni
                     @elseif($loan->rating == 5) Windykacja (zobowiązanie odzyskane)
                     @elseif($loan->rating == 6) Windykacja (zobowiązanie nieodzyskane)
                    @endif"
                   class="fa fa-2x fa-thumbs-down kt-font-danger"></i>
                @endif
            </td>
            <td class="kt-datatable__cell action-table"  style="width: 120px !important;">
                <span>
                    <span data-toggle="modal" data-target="#edit{{ $loan->id }}"><a data-toggle="tooltip" data-placement="top" title="Edytuj" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-edit"></i></a></span>
                     @if(Auth::user()->role == 1)
                    <span data-toggle="modal" data-target="#deleteModal{{ $loan->id }}"><a data-toggle="tooltip" data-placement="top" title="Usuń" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="fa fa-trash"></i></a></span>
                     @endif
                </span>
            </td>
        </tr>

        {{-- Edit modal--}}
        <div class="modal fade" id="edit{{ $loan->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pożyczka</h5>
                    </div>
                    <form method="post" class="kt-form update-institution-form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa projektu</strong></label>
                                <div class="col-8">
                                    <input class="form-control" value="{{ $loan->name }}" type="text" required name="name-edit{{ $loan->id }}" id="name-edit{{ $loan->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Cel finansowania</label>
                                <div class="col-8">
                                    <textarea class="form-control" type="text" rows="4" name="goal-edit{{ $loan->id }}" id="goal-edit{{ $loan->id }}">{{ $loan->goal }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Fundusz pożyczkowy</strong></label>
                                <div class="col-6">
                                    <select data-live-search="true" name="institutions-edit{{ $loan->id }}" id="institutions-edit{{ $loan->id }}" class="form-control institutions-edit">
                                        @if(Auth::user()->role == 1)
                                        <option value="{{ $loan->institution }}">{{ $loan->institution }}</option>
                                        @endif
                                        @foreach($institutions as $institution)
                                            <option value="{{$institution->name}}">{{ $institution->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(Auth::user()->role == 1)
                                <div class="col-2">
                                    <span data-toggle="tooltip" data-placement="top" title="Dodaj fundusz pożyczkowy" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i data-toggle="modal" data-target="#add_institution" class="fa fa-plus fa-2x"></i></span>
                                </div>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Pożyczkobiorca</strong></label>
                                <div class="col-6">
                                    <select data-live-search="true" name="borrowers-edit{{ $loan->id }}" id="borrowers-edit{{ $loan->id }}" class="form-control edit-selectpicker">
                                        <option value="{{ $loan->borrower }}">{{ $loan->borrower }}</option>
                                        @foreach($borrowers as $borrower)
                                            <option value="{{$borrower->name}}">{{ $borrower->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-2">
                                    <span data-toggle="tooltip" data-placement="top" title="Dodaj pożyczkobiorcę" class="btn  btn-clean btn-icon btn-icon-md"><i data-toggle="modal" data-target="#add_borrower" class="fa fa-plus fa-2x"></i></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Data zawarcia umowy</strong></label>
                                <div class="col-2">
                                    <input class="form-control date-input" value="{{ $loan->conclusion_date }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Data udzielonej pożyczki</strong></label>
                                <div class="col-2">
                                    <input class="form-control date-input" value="{{ $loan->award_date }}" type="text" name="award_date-edit{{ $loan->id }}" id="award_date-edit{{ $loan->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Okres</strong></label>
                                <div class="col-3 input-group">
                                    <input class="form-control" value="{{$loan->period}}" type="text" name="period-edit{{ $loan->id }}" id="period-edit{{ $loan->id }}">
                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2">mc</span></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Kwota pożyczki</strong></label>
                                <div class="col-3 input-group">
                                    <input class="form-control" value="{{ $loan->loan_amount }}" type="text" name="loan_amount-edit{{ $loan->id }}" id="loan_amount-edit{{ $loan->id }}">
                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"><strong>Kwota zobowiązania</strong></label>
                                <div class="col-3 input-group">
                                    <input class="form-control" value="{{ $loan->commitment_amount }}" type="text" name="commitment_amount-edit{{ $loan->id }}" id="commitment_amount-edit{{ $loan->id }}">
                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right"></label>
                                <div class="col-8">
                                    <label class="kt-checkbox de-minimis-label">
                                        <input type="checkbox" class="de-minimis-checkbox"> Czy udzielona jest pomocą de miminis
                                        <span></span>
                                    </label>
                                    <textarea style="display:none" class="form-control" rows="4" type="text" name="de_minimis-edit{{ $loan->id }}" id="de_minimis-edit{{ $loan->id }}">{{ $loan->de_minimis }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Status</label>
                                <div class="col-5">
                                    <select class="form-control edit-selectpicker" name="stats-edit{{ $loan->id }}" id="status-edit{{ $loan->id }}">
                                        <option @if($loan->status == 1) selected @endif data-content="<span class='btn btn-label-success btn-bold btn-sm p-0'>Aktywny</span>" value="1">Aktywny</option>
                                        <option @if($loan->status == 2) selected @endif data-content="<span class='btn btn-label-brand btn-bold btn-sm p-0'> Zakończony </label>" value="2">Zakończony</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Data spłaty</label>
                                <div class="col-2">
                                    <input class="form-control date-input" value="{{ $loan->payoff_date }}" type="text" name="payoff_date-edit{{ $loan->id }}" id="payoff_date-edit{{ $loan->id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-right">Ocena</label>
                                <div class="col-8">
                                    <select name="rating-edit{{ $loan->id }}" class="form-control edit-selectpicker" id="rating-edit{{ $loan->id }}">
                                        <option  @if($loan->rating == 1) selected @endif data-icon="fa fa-2x fa-thumbs-up kt-font-success" value="1">Spłata bez opóźnień</option>
                                        <option  @if($loan->rating == 2) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-warning" value="2">Opóźnienie w spłacie do 30 dni</option>
                                        <option  @if($loan->rating == 3) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="3">Opóźnienie w spłacie od 30 do 90 dni</option>
                                        <option  @if($loan->rating == 4) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="4">Opóźnienie w spłacie powyżej 90 dni</option>
                                        <option  @if($loan->rating == 5) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="5">Windykacja (zobowiązanie odzyskane)</option>
                                        <option  @if($loan->rating == 6) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="6">Windykacja (zobowiązanie nieodzyskane)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-left">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                            <button type="submit" class="btn btn-primary update-loan" id="update-{{ $loan->id }}">Zapisz</button>
                            <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal to delete-->
        <div class="modal fade" id="deleteModal{{ $loan->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Usuwanie pożyczki</h3>
                    </div>
                    <div class="modal-body">
                        <p>Czy na pewno chcesz usunąć pożyczkę?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                        <button type="button" class="btn btn-danger btn-destroy-user" id="{{ $loan->id }}">Usuń</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </tbody>
</table>

@if(count($loans) <= 0)
    <div class="text-center mt-3"><i class="fa fa-warning fa-3x mr-3"></i><p>Brak rekordów</p></div>
@endif

<div class="kt-datatable__pager kt-datatable--paging-loaded">
    {{ $loans->appends(\Request::except('page'))->links('layouts.pagination') }}

    <div class="justify-content-center">
        <form action="/" method="GET" class="">
            <div class="kt-pagination  kt-pagination--brand">
                <div class="kt-pagination__toolbar">
                    <span class="pagination__desc mr-2">Rekordów</span>
                    <select name="records" class="form-control kt-font-brand records" style="width: 60px;">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.edit-selectpicker, .institutions-edit').selectpicker();

    $('.btn-destroy-user').on('click', function(e){
        $('.kt-spinner').css('display', 'block');
        $('.btn-destroy-user').css('display', 'none');
        var id = $(this).attr('id');
        e.preventDefault();
        $.ajax({
            url: 'destroy-loan/' + id,
            method: 'POST',
            data: {
                '_token' : $('meta[name="csrf-token"]').attr('content')
            },
            success:function(data)
            {
                $('.kt-spinner').css('display', 'none');
                $('.btn-destroy-user').css('display', 'block');
                location.reload();
            }
        });
    });


    $('body').on('click', '.update-loan', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var id = $(this).attr('id').replace('update-', '');
        $('input').removeClass('is-invalid');
        $('.invalid-feedback').remove();
        $('.kt-spinner').css('display', 'block');
        $('.add-btn').hide();
        console.log(id);

        $.ajax({
            url: '/edit-loan/'+id,
            method: 'POST',
            data: {
                '_token'            : $('meta[name="csrf-token"]').attr('content'),
                'name'              : $('#name-edit' + id + '').val(),
                'goal'              : $('#goal-edit'+ id + '').val(),
                'institution'       : $('#institutions-edit'+ id + '').val(),
                'borrower'          : $('#borrowers-edit'+ id + '').val(),
                'conclusion_date'   : $('#conclusion_date-edit'+ id + '').val(),
                'award_date'        : $('#award_date-edit'+ id + '').val(),
                'period'            : $("#period-edit"+ id + '').val(),
                'loan_amount'       : $("#loan_amount-edit"+ id + '').val(),
                'commitment_amount' : $("#commitment_amount-edit"+ id + '').val(),
                'de_minimis'        : $('#de_minimis-edit'+ id + '').val(),
                'status'            : $('#status-edit'+ id + '').val(),
                'payoff_date'       : $('#payoff_date-edit'+ id + '').val(),
                'rating'            : $('#rating-edit'+ id + '').val()
            },
            success:function(data)
            {
                if(data.errors)
                {
                    toastr.error("Wypełnij poprawnie wszystkie pola.");
                    $.each(data.errors, function(key, value){
                        console.log(value);
                        $('input[name=' + key + '-edit'+id+']').addClass('is-invalid');
                    });
                }
                else
                {
                    toastr.success("Pomyślnie zapisano zmiany.");
                    setTimeout(function(){ location.reload(); }, 1000);
                }

                $('.kt-spinner').css('display', 'none');
                $('.add-btn').show();

            }
        });
    });
</script>
