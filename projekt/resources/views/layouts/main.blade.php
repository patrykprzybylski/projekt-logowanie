<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BIP</title>
    <link href="{{ asset('css/vendors.bundle.css') }}" rel="stylesheet">

    <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/skins/header/base/light.css') }}" rel="stylesheet">
    <link href="{{ asset('css/skins/header/menu/light.css') }}" rel="stylesheet">
    <link href="{{ asset('css/skins/brand/dark.css') }}" rel="stylesheet">
    <link href="{{ asset('css/skins/aside/dark.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <!--begin::Fonts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pl.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <!--end::Fonts -->

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

<!-- Fonts -->

</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed">
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="{{ url('/') }}">
            LOGO
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left kt-header-mobile__toolbar-toggler--active mr-3" id="kt_aside_mobile_toggler"><span></span></button>

        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown">
                <div class="kt-header__topbar-user">
                    <span class="kt-header__topbar-username" style="color:white !important;">{{ Auth::user()->name }} {{ Auth::user()->surname }}</span>
                </div>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('images/bg-1.jpg') }})">
                    <div class="kt-user-card__avatar">
                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                        <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ mb_substr(Auth::user()->name, 0, 1, "UTF-8") }}</span>
                    </div>
                    <div class="kt-user-card__name">
                        {{ Auth::user()->name }} {{ Auth::user()->surname }}
                    </div>
                </div>
                <div class="kt-notification">
                    <a href="{{ url('/edit/'.Auth::user()->id) }}" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Mój profil
                            </div>
                            <div class="kt-notification__item-time">
                                Ustawienia konta
                            </div>
                        </div>
                    </a>
                    <div class="kt-notification__custom kt-space-between">
                        <a class="btn btn-label btn-label-brand btn-sm btn-bold" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Wyloguj
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{--end mobile menu--}}

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-aside kt-aside--fixed kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
            <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand" style="">
                <div class="kt-aside__brand-logo">
                    <a href="{{ url('/') }}">
                        LOGO
                    </a>
                </div>
            </div>
            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                <div id="kt_aside_menu" class="kt-aside-menu kt-scroll ps ps--active-y" style="height: 662px; overflow: hidden;">
                    <ul class="kt-menu__nav ">
                        @if(Auth::user()->role == 1)
                        <li class="kt-menu__item"><a href="{{ url('/') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-user"></i></span><span class="kt-menu__link-text">Użytkownicy</span></a></li>
                        <li class="kt-menu__item"><a href="{{ url('/institutions') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-building"></i></span><span class="kt-menu__link-text">Instytucje pożyczkowe</span></a></li>
                        <li class="kt-menu__item"><a href="{{ url('/borrowers') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-user"></i></span><span class="kt-menu__link-text">Pożyczkobiorcy</span></a></li>
                        @endif
                        <li class="kt-menu__item"><a href="{{ url('/loans') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-money"></i></span><span class="kt-menu__link-text">Pożyczki</span></a></li>
                        <li class="kt-menu__item"><a href="{{ url('/search') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-search"></i></span><span class="kt-menu__link-text">Wyszukiwarka</span></a></li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper">
            <div class="kt-header kt-grid__item  kt-header--fixed ">
                <div class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
                </div>

                <div class="kt-header__topbar">
                    <div class="resp-menu ">
                        <div class="d-flex align-items-center mr-3">
                            <div class="dropdown ">
                                <div id="dropdownMenuButton" data-toggle="dropdown">
                                    <div class="hamburger"></div>
                                    <div class="hamburger"></div>
                                    <div class="hamburger"></div>
                                </div>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @if(Auth::user()->role == 1)
                                        <a class="dropdown-item" href="{{ url('/') }}">Użytkownicy</a>
                                        <a class="dropdown-item" href="{{ url('/institutions') }}">Instytucje pożyczkowe</a>
                                        <a class="dropdown-item" href="{{ url('/borrowers') }}">Pożyczkobiorcy</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ url('/loans') }}">Pożyczki</a>
                                    <a class="dropdown-item" href="{{ url('/search') }}">Wyszukiwarka</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-header__topbar-item kt-header__topbar-item--user">
                       <div class="kt-header__topbar-wrapper" data-toggle="dropdown">
                           <div class="kt-header__topbar-user">
                               <span class="kt-header__topbar-username">{{ Auth::user()->name }} {{ Auth::user()->surname }}</span>
                           </div>
                       </div>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('images/bg-1.jpg') }})">
                                <div class="kt-user-card__avatar">
                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ mb_substr(Auth::user()->name, 0, 1, "UTF-8") }}</span>
                                </div>
                                <div class="kt-user-card__name">
                                    {{ Auth::user()->name }} {{ Auth::user()->surname }}
                                </div>
                            </div>
                            <div class="kt-notification">
                                <a href="{{ url('/edit/'.Auth::user()->id) }}" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-calendar-3 kt-font-success"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title kt-font-bold">
                                            Mój profil
                                        </div>
                                        <div class="kt-notification__item-time">
                                            Ustawienia konta
                                        </div>
                                    </div>
                                </a>
                                <div class="kt-notification__custom kt-space-between">
                                    <a class="btn btn-label btn-label-brand btn-sm btn-bold" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Wyloguj
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @yield('content')
{{--            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">--}}
{{--                <div class="kt-subheader  kt-grid__item" id="kt_subheader">--}}
{{--                    <div class="kt-container  kt-container--fluid ">--}}
{{--                        <div class="kt-subheader__main">--}}
{{--                            <h3 class="kt-subheader__title"></h3>--}}
{{--                            <div class="kt-subheader__breadcrumbs"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">--}}
{{--                    <div class="row">--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

    </div>
</div>
<script>
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

$('#kt_aside_mobile_toggler').on('click', function(){
    $('#kt_aside').toggleClass('kt-aside--on');
});

</script>
<style>
    .modal {
        overflow-y:auto;
    }

    .active-sort {
        color: #5d78ff !important;
    }

    .sorting {
        color: #595d6e;
        font-weight: 500;
    }

    .datepicker {
        z-index: 100000 !important;
    }

    .resp-menu{
        display: none;
    }

    .hamburger {
        width: 35px;
        height: 3px;
        background-color: #6c7293;
        margin: 5px 0;
    }

    @media screen and (max-width: 1025px) {
        .resp-menu {
            display: flex;
        }
    }
</style>
</body>
</html>
