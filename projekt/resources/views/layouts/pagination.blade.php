@if ($paginator->hasPages())
    <div class="kt-pagination  kt-pagination--brand">
        <ul class="kt-pagination__links">
            <li class="kt-pagination__link--first">
                <a class="pag-link" href="{{ $paginator->url(1) }}"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
            </li>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled kt-pagination__link--next"><a class=""><i class="fa fa-angle-left kt-font-brand"></i></a></li>
            @else
                <li class="kt-pagination__link--next"><a class="pag-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fa fa-angle-left kt-font-brand"></i></a></li>
            @endif


            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif


                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="kt-pagination__link--active"><a>{{ $page }}</a></li>
                        @else
                            <li><a class="pag-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach


            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="kt-pagination__link--prev"><a class="pag-link" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fa fa-angle-right kt-font-brand"></i></a></li>
            @else
                <li class="disabled kt-pagination__link--prev"><a><i class="fa fa-angle-right kt-font-brand"></i></a></li>
            @endif


            <li class="kt-pagination__link--last">
                <a class="pag-link" href="{{ $paginator->url($paginator->lastPage()) }}"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
            </li>

        </ul>
    </div>

@endif