@extends('layouts.main')

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Użytkownicy</h3>
                    <div class="kt-subheader__breadcrumbs">
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ url('/') }}" class="kt-subheader__breadcrumbs-link">
                            Lista użytkowników
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">

                <div class="col-xl-12 order-lg-2 order-xl-1">

                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Filtrowanie
                            </h3>
                            <div class="show-filters ml-3">
                                <span style="display: none;" class="btn btn-label-primary fraza-btn">Fraza<i style="cursor: pointer;" class="fa fa-close ml-1 remove-fraza"></i></span>
                            </div>
                        </div>

                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <a class="btn btn-sm btn-icon btn-clean btn-icon-md" data-toggle="collapse" href="#filters"><i class="fa fa-angle-down"></i></a>
                            </div>
                        </div>
                    </div>
                    <form action="/" method="GET" class="collapse" id="filters">
                    <div class="kt-portlet__body" >
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="active">Aktywność</label>
                                    <select name="active" class="form-control" id="active">
                                        <option value="">Wszyscy</option>
                                        <option value="active">Aktywne</option>
                                        <option value="not">Nieaktywne</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="role">Rola</label>
                                    <select name="role" class="form-control" id="role">
                                        <option value="">Wszyscy</option>
                                        <option value="1">Administrator</option>
                                        <option value="2">Użytkownik</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <button type="submit" class="btn btn-primary filter-users">Filtruj</button>
                        <button class="btn btn-secondary reset-filters">Wyczyść</button>
                    </div>
                    </form>
                </div>
                </div>

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Lista użytkowników </h3> <span class="ml-2">(</span><span class="count">{{ $allUsers }}</span>/<span>{{ $allUsers }}</span>)
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <a href="{{ url('/create') }}" class="btn btn-brand btn-icon-sm"><i class="fa fa-plus fa-2x"></i>Dodaj użytkownika</a>
                            </div>
                        </div>
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm kt-margin-t-20">
                            <div class="form-group kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <form action="/" method="GET" class="">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control user-name" placeholder="Szukaj..." id="generalSearch" name="userName">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="fa fa-search"></i></span>
                                    </span>
                                    </div>
                                </form>
                            </div>

                            <div class="form-group kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">

                            </div>
                        </div>
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded kt-margin-t-20">
                           @include('usersList')
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        // $(window).on('hashchange', function() {
        //     if (window.location.hash) {
        //         var page = window.location.hash.replace('#', '');
        //         if (page == Number.NaN || page <= 0) {
        //             return false;
        //         } else {
        //             getUsers(page);
        //         }
        //     }
        // });
        $(document).ready(function() {
            // $(document).on('click', '.pagination a', function (e) {
            //     getUsers($(this).attr('href').split('page=')[1]);
            //     e.preventDefault();
            // });

            $('.filter-users').on('click', function(e){
                e.preventDefault();
                $.ajax({
                    url : '/',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'role'      : $( "#role" ).val(),
                        'active'    : $( "#active" ).val(),
                    }
                }).done(function (data) {

                    $('.kt-datatable').html(data);
                    //show filters
                    $('.show-filters').html('');
                    if($( "#role" ).val())
                    {
                        var val = $('#role option:selected').text();
                        $('.show-filters').append(' <span class="btn btn-label-primary role-btn">Rola<i style="cursor: pointer;" class="fa fa-close ml-1 remove-role"></i></span> ')
                    }
                    if($( "#active" ).val())
                    {
                        var val = $('#active option:selected').text();
                        $('.show-filters').append(' <span class="btn btn-label-primary active-btn">Aktywność<i style="cursor: pointer;" class="fa fa-close ml-1 remove-active"></i></span>')
                    }

                    countUsers();
                }).fail(function () {
                    alert('Nie można wczytać uzytkowników.');
                });
            });

            $('body').on('change', '.records', function(e){
                e.preventDefault();
                $.ajax({
                    url : '/',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'records'      : $( ".records" ).val(),
                    }
                }).done(function (data){
                    $('.kt-datatable').html(data);
                });
            });

            $('.user-name').on('keyup', function(e){
                e.preventDefault();
                $.ajax({
                    url : '/',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: {
                        'userName'  : $( ".user-name" ).val(),
                    }
                }).done(function (data) {
                    $('.kt-datatable').html(data);
                    $('.fraza-btn').css('display', 'inline');
                    countUsers();
                }).fail(function () {
                    alert('Nie można wczytać uzytkowników.');
                });
            });

            $('.reset-filters').on('click', function(e){
                e.preventDefault();
                $( "#role" ).val('');
                $( "#active" ).val('');
                $.ajax({
                    url : '/',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (data) {
                    $('.kt-datatable').html(data);
                    $('.show-filters').html('');
                    countUsers();
                }).fail(function () {
                    alert('Nie można wczytać uzytkowników.');
                });
            });
        });

        function getUsers(page) {
            $.ajax({
                url : '?page=' + page,
                dataType: 'json',

            }).done(function (data) {
                 $('.kt-datatable').html(data);
                location.hash = page;
            }).fail(function () {
                alert('Nie można wczytać uzytkowników.');
            });
        }


        $('body').on('click', '.remove-role', function(e){
            console.log('clickd');
            $( "#role" ).val('');
            $('.role-btn').remove();
            $.ajax({
                url : '/',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'active'    : $( "#active" ).val(),
                }
            }).done(function (data) {
                $('.kt-datatable').html(data);
                countUsers();
            });
        });

        $('body').on('click', '.remove-active', function(e){
            console.log('clickd');
            $( "#active" ).val('');
            $('.active-btn').remove();
            $.ajax({
                url : '/',
                dataType: 'json',
                contentType: 'application/json',
                data: {
                    'role'    : $( "#role" ).val(),
                }
            }).done(function (data) {
                $('.kt-datatable').html(data);
                countUsers();
            });
        });

        $('body').on('click', '.remove-fraza', function(e){
            $( ".user-name" ).val('');
            e.preventDefault();
            $.ajax({
                url : '/',
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                $('.fraza-btn').css('display', 'none');
                countUsers();
            }).fail(function () {
                alert('Nie można wczytać uzytkowników.');
            });
        });

        $('body').on('click', '.sorting', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
                console.log('table');

            });
        });


        $('body').on('click', '.pag-link', function(e)
        {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url : link,
                dataType: 'json',
                contentType: 'application/json',
            }).done(function (data) {
                $('.kt-datatable').html(data);
            });
        });


        function countUsers()
        {
            var nr = $('table').attr('id');
            if(nr)
            {
                $('.count').html(nr);
            }
        }

    </script>
@endsection