<div class="details">
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden-">
                        <img src="{{ asset('images/3.png') }}" alt="image">
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a class="kt-widget__title borrower-name">@isset($borrower) {{ $borrower->name }} @endisset</a>
                            @isset($borrower) <a class="btn btn-secondary" href="{{ url('/generate-pdf/' . $borrower->nip) }}"><i class="fa fa-print"></i>Drukuj</a> @endisset
                        </div>

                        <div class="kt-widget__info">
                            <div class="kt-widget__desc borrower-address">
                                @isset($borrower)
                                    <span>{{ $borrower->address }}, {{ $borrower->post_code }} {{ $borrower->city }}</span> <br>
                                    <span>NIP: {{ $borrower->nip }}</span>
                                @endisset
                            </div>

                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-file-2"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Ilość pożyczek</span>
                            <span class="kt-widget__value">@if(isset($loans)) {{ count($loans) }} @else 0 @endif</span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-piggy-bank"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Wartość pożyczek</span>
                            <span class="kt-widget__value">@if(isset($loansAmount)) {{ $loansAmount}} zł @else 0 @endif</span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="fa fa-3x fa-thumbs-up kt-font-success"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Pozytywne</span>
                            <span class="kt-widget__value">@if(isset($positiveLoans)) {{ count($positiveLoans) }} @else 0 @endif</span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="fa fa-3x fa-thumbs-down kt-font-danger"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Negatywne</span>
                            <span class="kt-widget__value">@if(isset($negativeLoans)) {{ count($negativeLoans) }} @else 0 @endif</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title d-flex align-items-center"><i class="fa fa-thumbs-up fa-2x kt-font-success mr-2"></i>Pozytywne</h3>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="kt-widget4">
                        @if(isset($positiveLoans))
                            @foreach($positiveLoans as $loan)
                                <div class="kt-widget4__item">
                                    <span class="kt-widget4__title">
                                        <span style="cursor: pointer;" data-toggle="modal" data-target="#info{{ $loan->id }}"><strong>{{ $loan->name }}</strong></span><br>
                                        <span>{{ $loan->institution }}</span>
                                    </span>
                                    <span class="kt-widget4__number kt-font-info mr-2">
                                        {{ $loan->commitment_amount }} zł

                                    </span>
                                    <span><i class="fa fa-thumbs-up fa-2x kt-font-success" data-toggle="tooltip" data-placement="top" data-original-title="Spłata bez opóźnień"></i></span>
                                </div>

                            {{--loan modal --}}
                                <div class="modal fade" id="info{{ $loan->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Pożyczka</h5>
                                            </div>


                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa projektu</strong></label>
                                                        <div class="col-8">
                                                            <input class="form-control" value="{{ $loan->name }}" type="text" required name="name-edit{{ $loan->id }}" id="name-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right">Cel finansowania</label>
                                                        <div class="col-8">
                                                            <textarea class="form-control" type="text" rows="4" name="goal-edit{{ $loan->id }}" id="goal-edit{{ $loan->id }}">{{ $loan->goal }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Fundusz pożyczkowy</strong></label>
                                                        <div class="col-6">
                                                            <input class="form-control date-input" value="  {{ $loan->institution }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Pożyczkobiorca</strong></label>
                                                        <div class="col-6">
                                                            <input class="form-control date-input" value="  {{ $loan->borrower }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Data zawarcia umowy</strong></label>
                                                        <div class="col-2">
                                                            <input class="form-control date-input" value="{{ $loan->conclusion_date }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Data udzielonej pożyczki</strong></label>
                                                        <div class="col-2">
                                                            <input class="form-control date-input" value="{{ $loan->award_date }}" type="text" name="award_date-edit{{ $loan->id }}" id="award_date-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Okres</strong></label>
                                                        <div class="col-3 input-group">
                                                            <input class="form-control" value="{{$loan->period}}" type="text" name="period-edit{{ $loan->id }}" id="period-edit{{ $loan->id }}">
                                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2">mc</span></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Kwota pożyczki</strong></label>
                                                        <div class="col-3 input-group">
                                                            <input class="form-control" value="{{ $loan->loan_amount }}" type="text" name="loan_amount-edit{{ $loan->id }}" id="loan_amount-edit{{ $loan->id }}">
                                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right"><strong>Kwota zobowiązania</strong></label>
                                                        <div class="col-3 input-group">
                                                            <input class="form-control" value="{{ $loan->commitment_amount }}" type="text" name="commitment_amount-edit{{ $loan->id }}" id="commitment_amount-edit{{ $loan->id }}">
                                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                                        </div>
                                                    </div>

{{--                                                    <div class="form-group row">--}}
{{--                                                        <label for="name" class="col-4 col-form-label text-right"></label>--}}
{{--                                                        <div class="col-8">--}}
{{--                                                            <label class="kt-checkbox de-minimis-label">--}}
{{--                                                                <input type="checkbox" class="de-minimis-checkbox"> Czy udzielona jest pomocą de miminis--}}
{{--                                                                <span></span>--}}
{{--                                                            </label>--}}
{{--                                                            <textarea style="display:none" class="form-control" rows="4" type="text" name="de_minimis-edit{{ $loan->id }}" id="de_minimis-edit{{ $loan->id }}">{{ $loan->de_minimis }}</textarea>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right">Status</label>
                                                        <div class="col-5">
                                                            <select class="form-control edit-selectpicker" name="stats-edit{{ $loan->id }}" id="status-edit{{ $loan->id }}">
                                                                <option @if($loan->status == 1) selected @endif data-content="<span class='btn btn-label-success btn-bold btn-sm p-0'>Aktywny</span>" value="1">Aktywny</option>
                                                                <option @if($loan->status == 2) selected @endif data-content="<span class='btn btn-label-brand btn-bold btn-sm p-0'> Zakończony </label>" value="2">Zakończony</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right">Data spłaty</label>
                                                        <div class="col-2">
                                                            <input class="form-control date-input" value="{{ $loan->payoff_date }}" type="text" name="payoff_date-edit{{ $loan->id }}" id="payoff_date-edit{{ $loan->id }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label text-right">Ocena</label>
                                                        <div class="col-8">
                                                            <select name="rating-edit{{ $loan->id }}" class="form-control edit-selectpicker" id="rating-edit{{ $loan->id }}">
                                                                <option  @if($loan->rating == 1) selected @endif data-icon="fa fa-2x fa-thumbs-up kt-font-success" value="1">Spłata bez opóźnień</option>
                                                                <option  @if($loan->rating == 2) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-warning" value="2">Opóźnienie w spłacie do 30 dni</option>
                                                                <option  @if($loan->rating == 3) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="3">Opóźnienie w spłacie od 30 do 90 dni</option>
                                                                <option  @if($loan->rating == 4) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="4">Opóźnienie w spłacie powyżej 90 dni</option>
                                                                <option  @if($loan->rating == 5) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="5">Windykacja (zobowiązanie odzyskane)</option>
                                                                <option  @if($loan->rating == 6) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="6">Windykacja (zobowiązanie nieodzyskane)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer text-left">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>

                                                    <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                                                </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        @else
                            <div class="text-center">
                                <i class="fa fa-warning fa-3x mr-3"></i>
                                <p>Brak rekordów</p>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title d-flex align-items-center"><i class="fa fa-2x fa-thumbs-down kt-font-danger mr-2"></i>Negatywne</h3>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="kt-widget4">
                        @if(isset($negativeLoans))
                            @foreach($negativeLoans as $loan)
                                <div class="kt-widget4__item">
                                    <span class="kt-widget4__title">
                                        <span style="cursor: pointer;" data-toggle="modal" data-target="#info{{ $loan->id }}"><strong>{{ $loan->name }}</strong></span><br>
                                        <span>{{ $loan->institution }}</span>
                                    </span>
                                    <span class="kt-widget4__number kt-font-info mr-2">
                                        {{ $loan->commitment_amount }} zł

                                    </span>
                                    @if($loan->rating == 2)
                                    <span><i class="fa fa-2x fa-thumbs-down kt-font-warning" data-toggle="tooltip" data-placement="top" data-original-title="Opóźnienie w spłacie do 30 dni"></i></span>
                                    @else
                                        <span><i class="fa fa-2x fa-thumbs-down kt-font-danger" data-toggle="tooltip" data-placement="top" data-original-title="
                                        @if($loan->rating == 3) Opóźnienie w spłacie od 30 do 90 dni
                                         @elseif($loan->rating == 4) Opóźnienie w spłacie powyżej 90 dni
                                         @elseif($loan->rating == 5) Windykacja (zobowiązanie odzyskane)
                                         @elseif($loan->rating == 6) Windykacja (zobowiązanie nieodzyskane)
                                        @endif"></i></span>
                                    @endif
                                </div>

                                {{--loan modal --}}
                                <div class="modal fade" id="info{{ $loan->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Pożyczka</h5>
                                            </div>


                                            <div class="modal-body">
                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Nazwa projektu</strong></label>
                                                    <div class="col-8">
                                                        <input class="form-control" value="{{ $loan->name }}" type="text" required name="name-edit{{ $loan->id }}" id="name-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right">Cel finansowania</label>
                                                    <div class="col-8">
                                                        <textarea class="form-control" type="text" rows="4" name="goal-edit{{ $loan->id }}" id="goal-edit{{ $loan->id }}">{{ $loan->goal }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Fundusz pożyczkowy</strong></label>
                                                    <div class="col-6">
                                                        <input class="form-control date-input" value="  {{ $loan->institution }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Pożyczkobiorca</strong></label>
                                                    <div class="col-6">
                                                        <input class="form-control date-input" value="  {{ $loan->borrower }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Data zawarcia umowy</strong></label>
                                                    <div class="col-2">
                                                        <input class="form-control date-input" value="{{ $loan->conclusion_date }}" type="text" name="conclusion_date-edit{{ $loan->id }}" id="conclusion_date-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Data udzielonej pożyczki</strong></label>
                                                    <div class="col-2">
                                                        <input class="form-control date-input" value="{{ $loan->award_date }}" type="text" name="award_date-edit{{ $loan->id }}" id="award_date-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Okres</strong></label>
                                                    <div class="col-3 input-group">
                                                        <input class="form-control" value="{{$loan->period}}" type="text" name="period-edit{{ $loan->id }}" id="period-edit{{ $loan->id }}">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2">mc</span></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Kwota pożyczki</strong></label>
                                                    <div class="col-3 input-group">
                                                        <input class="form-control" value="{{ $loan->loan_amount }}" type="text" name="loan_amount-edit{{ $loan->id }}" id="loan_amount-edit{{ $loan->id }}">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right"><strong>Kwota zobowiązania</strong></label>
                                                    <div class="col-3 input-group">
                                                        <input class="form-control" value="{{ $loan->commitment_amount }}" type="text" name="commitment_amount-edit{{ $loan->id }}" id="commitment_amount-edit{{ $loan->id }}">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2">zł</span></div>
                                                    </div>
                                                </div>

                                                {{--                                                    <div class="form-group row">--}}
                                                {{--                                                        <label for="name" class="col-4 col-form-label text-right"></label>--}}
                                                {{--                                                        <div class="col-8">--}}
                                                {{--                                                            <label class="kt-checkbox de-minimis-label">--}}
                                                {{--                                                                <input type="checkbox" class="de-minimis-checkbox"> Czy udzielona jest pomocą de miminis--}}
                                                {{--                                                                <span></span>--}}
                                                {{--                                                            </label>--}}
                                                {{--                                                            <textarea style="display:none" class="form-control" rows="4" type="text" name="de_minimis-edit{{ $loan->id }}" id="de_minimis-edit{{ $loan->id }}">{{ $loan->de_minimis }}</textarea>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right">Status</label>
                                                    <div class="col-5">
                                                        <select class="form-control edit-selectpicker" name="stats-edit{{ $loan->id }}" id="status-edit{{ $loan->id }}">
                                                            <option @if($loan->status == 1) selected @endif data-content="<span class='btn btn-label-success btn-bold btn-sm p-0'>Aktywny</span>" value="1">Aktywny</option>
                                                            <option @if($loan->status == 2) selected @endif data-content="<span class='btn btn-label-brand btn-bold btn-sm p-0'> Zakończony </label>" value="2">Zakończony</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right">Data spłaty</label>
                                                    <div class="col-2">
                                                        <input class="form-control date-input" value="{{ $loan->payoff_date }}" type="text" name="payoff_date-edit{{ $loan->id }}" id="payoff_date-edit{{ $loan->id }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-4 col-form-label text-right">Ocena</label>
                                                    <div class="col-8">
                                                        <select name="rating-edit{{ $loan->id }}" class="form-control edit-selectpicker" id="rating-edit{{ $loan->id }}">
                                                            <option  @if($loan->rating == 1) selected @endif data-icon="fa fa-2x fa-thumbs-up kt-font-success" value="1">Spłata bez opóźnień</option>
                                                            <option  @if($loan->rating == 2) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="2">Opóźnienie w spłacie do 30 dni</option>
                                                            <option  @if($loan->rating == 3) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="3">Opóźnienie w spłacie od 30 do 90 dni</option>
                                                            <option  @if($loan->rating == 4) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="4">Opóźnienie w spłacie powyżej 90 dni</option>
                                                            <option  @if($loan->rating == 5) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="5">Windykacja (zobowiązanie odzyskane)</option>
                                                            <option  @if($loan->rating == 6) selected @endif data-icon="fa fa-2x fa-thumbs-down kt-font-danger" value="6">Windykacja (zobowiązanie nieodzyskane)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer text-left">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>

                                                <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="text-center">
                                <i class="fa fa-warning fa-3x mr-3"></i>
                                <p>Brak rekordów</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.edit-selectpicker').selectpicker();
</script>
