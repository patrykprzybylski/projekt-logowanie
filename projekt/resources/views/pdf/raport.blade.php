<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raport</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        body {font-family: DejaVu Sans, sans-serif; font-size: 11px; }
        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 150px;

        }
    </style>
</head>
<body>

<table class="table table-bordered main-table">
    <tbody class="text-center">
        <tr class="bg-light">
            <td colspan="3"><strong>RAPORT BIURA INFORMACJI POŻYCZKOWYCH</strong></td>
        </tr>
        <tr>
            <td class="p-1"  style="width: 40%; vertical-align: middle !important; padding: 0 !important; margin: 0 !important;" rowspan="3">
                <img style="height: 80px;" src="{{ public_path() . '/images/logo.jpg' }}">
            </td>
            <td colspan="2">Przedsiębiorca:<br/>
            <span><strong>{{ $borrower->name }}</strong></span>
            </td>
        </tr>
        <tr>
            <td class="bg-light p-1"><strong>Raport z dnia:</strong></td>
            <td class="p-1">{{ date('Y-m-d') }} o {{ date('H:i:s') }}</td>
        </tr>
        <tr>
            <td class="bg-light p-1"><strong>NIP:</strong></td>
            <td class="p-1">{{ $borrower->nip}}</td>
        </tr>
    </tbody>
</table>

<table class="table table-bordered mt-2">
    <tbody class="text-center">
    <tr class="bg-light">
        <td colspan="4"><strong>PRZEDSIĘBIORCA</strong></td>
    </tr>
    <tr>
        <td style="width: 50%;" class="text-right p-1" colspan="2">Nazwa:</td>
        <td style="width: 50%;" class="p-1" colspan="2">{{ $borrower->name }}</td>
    </tr>
    <tr>
        <td style="width: 50%;" class="text-right p-1" colspan="2">Adres:</td>
        <td style="width: 50%;" class="p-1" colspan="2">{{ $borrower->address }}</td>
    </tr>
    <tr>
        <td style="width: 50%;" class="text-right p-1" colspan="2">NIP:</td>
        <td style="width: 50%;" class="p-1" colspan="2">{{ $borrower->nip }}</td>
    </tr>
    <tr>
        <td class="bg-light"><strong>Ilość pożyczek</strong></td>
        <td class="bg-light"><strong>Wartość pożyczek</strong></td>
        <td class="bg-success text-white"><strong>Pozytywnych</strong></td>
        <td class="bg-danger text-white"><strong>Negatywnych</strong></td>
    </tr>
    <tr>
        <td>{{ count($loans) }}</td>
        <td>{{ $loansAmount }} zł</td>
        <td class="text-success text-white">{{ count($positiveLoans) }}</td>
        <td class="text-danger">{{ count($negativeLoans) }}</td>
    </tr>
    </tbody>
</table>

<p><strong>Biuro Informacji Pożyczkowej potwierdza, że przedsiębiorca posiada zobowiązania prezentowane poniżej:</strong></p>

    <table class="table">
        <tbody class="text-center">
        <tr>
            <td style="width: 50%;" class="bg-success text-white" colspan="2"><strong>Pozytywne({{ count($positiveLoans) }})</strong></td>
            <td style="width: 50%;" class="bg-danger text-white" colspan="2"><strong>Negatywne({{ count($negativeLoans) }})</strong></td>
        </tr>
            <tr>
                <td style="width: 50%;" class="p-0" colspan="2">
                    <table class="table table-bordered">
                        @foreach($positiveLoans as $loan)
                            <tr>
                                <td class="text-left">
                                    <strong>{{ $loan->name }}</strong><br/>
                                    {{ $loan->institution }}
                                </td>
                                <td>
                                    {{ $loan->commitment_amount }} zł
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
                <td style="width: 50%;" class="p-0" colspan="2">
                    <table class="table table-bordered">
                        @foreach($negativeLoans as $loan)
                            <tr>
                                <td class="text-left">
                                    <strong>{{ $loan->name }}</strong><br/>
                                    {{ $loan->institution }}
                                </td>
                                <td>
                                    {{ $loan->commitment_amount }} zł
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

<footer>
    <p class="text-secondary text-center">Biuro Informacji Kredytowej sp. z o.o., ul. Prosta 2, 70-777 Szczecin, tel.911111111 <br/>
    KRS: 00001101111, Sad Rejonowy Szczecin, XIII Wydział Gospodarczy<br/>
    Kapitał zakładowy 50.000 zł opłacony w całości, NIP: 222-77-77-333<br/><br/>

    www bipraport.pl</p>
</footer>

</body>
</html>

