@extends('layouts.main')

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Użytkownicy</h3>
                    <div class="kt-subheader__breadcrumbs">
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ url('/create') }}" class="kt-subheader__breadcrumbs-link">
                            Dodawanie użytkownika
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-toolbar">
                                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="" role="tab" aria-selected="true">
                                            <i class="flaticon2-calendar-3"></i> Informacje podstawowe
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <form method="post" id="store-form" class="kt-form">
                            @csrf
                            <div class="kt-portlet__body">
                                <div class="kt-section">
                                    <div class="row">
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-6">
                                            <h3 class="kt-section__title kt-section__title-sm">Informacje osobiste:</h3>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label text-right">Zdjęcie:</label>
                                        <div class="col-lg-6">

                                            <div class="kt-avatar kt-avatar--outline" id="kt_apps_user_add_avatar">
                                                <div class="kt-avatar__holder" style="background-image: url({{ asset('avatar.png') }})"></div>
                                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Zmień zdjęcie">
                                                        <i class="fa fa-pen"></i>
                                                        <input type="file" id="select_file" name="select_file" accept=".png, .jpg, .jpeg">
                                                    </label>
                                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                           <i class="fa fa-times"></i>
                                                    </span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label text-right"><strong>Imię:</strong></label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="name" id="name">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label text-right"><strong>Nazwisko:</strong></label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="surname" id="surname">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label text-right">Telefon:</label>
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                <input type="text" class="form-control" name="phone" id="phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group-last row">
                                        <label class="col-lg-3 col-form-label text-right"><strong>E-mail:</strong></label>
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                                <input type="email" class="form-control" name="email" id="email">
                                            </div>
                                            <p>Adres e-mail służy do logowania się w systemie</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-portlet__foot">
                                <div class="row form-group">
                                    <label class="col-lg-3 col-form-label text-right"><strong>Hasło:</strong></label>
                                    <div class="col-lg-6">
                                        <input type="password" class="form-control" name="password" id="password">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-3 col-form-label text-right"><strong>Powtórz hasło:</strong></label>
                                    <div class="col-lg-6">
                                        <input type="password" class="form-control" name="repeat" id="repeat">
                                    </div>
                                </div>
                            </div>

                            <div class="kt-portlet__foot">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">Rola:</label>
                                    <div class="col-lg-6">
                                        <div class="kt-radio-list">
                                            <label class="kt-radio">
                                                <input type="radio" name="role" value="1" class="role-input"> Administrator
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" checked="checked" value="2" name="role" class="role-input"> Fundusz
                                                <span></span>
                                            </label>

                                            <div class="mt-1 institutions-container">
                                                <select data-live-search="true" name="institutions" id="institutions" class="form-control">
                                                    <option value="">Wybierz</option>
                                                    @foreach($institutions as $institution)
                                                        <option value="{{$institution->id}}">{{ $institution->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-6">
                                            <div style="display: none;" class="kt-spinner kt-spinner--lg kt-spinner--success"></div>
                                            <button type="submit" class="btn btn-brand btn-bold">Dodaj</button>
                                            <a href="{{ url('/') }}" class="btn btn-clean btn-bold">Powrót</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            $('#institutions').selectpicker();
            showInstitutions();
            $('.kt-radio').on('click', function(){
                showInstitutions();
            });

            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $('#store-form').on('submit', function(e) {
                e.preventDefault();
                $('input').removeClass('is-invalid');
                $('.invalid-feedback').remove();
                $('.kt-spinner').css('display', 'block');
                $('.add-btn').css('display', 'none');
                var file_data = $('#select_file').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
                form_data.append('name', $('#name').val());
                form_data.append('surname', $('#surname').val());
                form_data.append('email', $('#email').val());
                form_data.append('password', $('#password').val());
                form_data.append('phone', $('#phone').val());
                form_data.append('role',$(".role-input:checked").val());
                form_data.append('institution', $('#institutions').val());

                $.ajax({
                    url: '/store-user',
                    method: 'POST',
                    data: form_data,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            console.log(data);
                            //sweet alert
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Gratulacje',
                                text: 'Użytkownik został dodany',
                                showConfirmButton: true
                            })
                        }
                        if(data.errors)
                        {
                            toastr.error("Imię jest wymagane<br/>Nazwisko jest wymagane<br/>Email jest wymagany<br/>Hasło jest wymagane<br/>");
                            $.each(data.errors, function(key, value){
                                $('input[name=' + key + ']').addClass('is-invalid');
                            });
                        }

                        $('.kt-spinner').css('display', 'none');
                        $('.add-btn').css('display', 'block');

                    }
                });
            });
        });

        $(function () {
            $('[data-toggle="kt-tooltip"]').tooltip()
        });


        function showInstitutions()
        {
            let val = $('.role-input:checked').val();
            if(val === '1')
            {
                $('.institutions-container').css('display', 'none');
            }
            if(val === '2')
            {
                $('.institutions-container').css('display', 'block');
            }
        }
    </script>

@endsection
