<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('goal')->nullable();
            $table->string('institution');
            $table->string('borrower');
            $table->integer('borrower_id');
            $table->date('conclusion_date');
            $table->date('award_date');
            $table->string('period');
            $table->string('loan_amount');
            $table->string('commitment_amount');
            $table->string('de_minimis')->nullable();
            $table->string('status')->nullable();
            $table->date('payoff_date')->nullable();
            $table->string('rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
