<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['admin']], function () {
        Route::any('/', 'HomeController@index')->name('index')->middleware('admin');
        Route::get('/create', 'HomeController@create')->name('create');
        Route::post('/store-user', 'HomeController@store')->name('store');
        Route::get('/home', 'HomeController@index')->name('index');

        Route::post('/destroy/{id}', 'HomeController@destroy')->name('destroy');
        Route::any('/users', 'HomeController@getUsers')->name('getUsers');
        Route::get('/institutions', 'InstitutionsController@index')->name('index');
        Route::post('/institutions', 'InstitutionsController@store')->name('store');
        Route::post('/destroy-institution/{id}', 'InstitutionsController@destroy')->name('destroy');
        Route::post('/edit-institution/{id}', 'InstitutionsController@update')->name('update');
        Route::get('/borrowers', 'BorrowersController@index')->name('index');
        Route::post('/borrowers', 'BorrowersController@store')->name('store');
        Route::post('/destroy-borrower/{id}', 'BorrowersController@destroy')->name('destroy');
        Route::post('/edit-borrower/{id}', 'BorrowersController@update')->name('update');
    });

    Route::get('/edit/{id}', 'HomeController@edit')->name('edit');
    Route::post('/edit/{id}', 'HomeController@update')->name('update');
    Route::post('/changePassword/{id}','HomeController@changePassword')->name('changePassword');
    Route::post('/ajax_upload/upload', 'HomeController@uploadFile')->name('ajaxupload.uploadFile');
    Route::any('/ajax_delete', 'HomeController@deleteAvatar')->name('ajaxdelete');

    Route::get('/loans', 'LoansController@index')->name('index');
    Route::post('/loans', 'LoansController@store')->name('store');
    Route::post('/destroy-loan/{id}', 'LoansController@destroy')->name('destroy');
    Route::get('/institution-details/{name}', 'LoansController@getInstitutionDetails')->name('getInstitutionDetails');
    Route::get('/borrower-details/{name}', 'LoansController@getBorrowerDetails')->name('getBorrowerDetails');
    Route::get('/filter-loans', 'LoansController@filter')->name('filter');
    Route::post('/edit-loan/{id}', 'LoansController@update')->name('update');
    Route::get('/search', 'SearchController@index')->name('index');
    Route::get('/search-loans', 'SearchController@search')->name('search');

    Route::get('/generate-pdf/{nip}', 'SearchController@generatePDF')->name('generatePDF');


    Route::any('/gus', 'GusApiController@checkNip')->name('checkNip');

});



Auth::routes();

